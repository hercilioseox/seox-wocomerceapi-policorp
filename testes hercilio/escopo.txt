# Remover shipping e billing:
	add_filter( 'woocommerce_cart_needs_payment', '__return_false' );

	add_filter( 'woocommerce_cart_needs_shipping', '__return_false' );

	add_action( 'woocommerce_checkout_order_review', 'woocommerce_notes_orders', 15 );
	function woocommerce_notes_orders() {
		wc_get_template( 'checkout/form-shipping.php', array(
				'checkout' => WC()->checkout(),
			) );
	}
#

# script para alterar style.
	esconder label detalhes de cobrança:
		add_action( 'wp_head', 'include_styles' );

		function include_styles() {
			if ( is_checkout() ) {
			echo '
					<style>
						.woocommerce-billing-fields h3 {
							display: none;
						}
					</style>
			';
			}
		}
#	

# Mostrar id do caravell

	add_filter ( 'woocommerce_order_number', 'mc_alter_pedido_id_field' );
	function mc_alter_pedido_id_field($id){
		$order = wc_get_order( $id);
		$item_data = $order;

		echo '<pre>'; print_r($item_data); echo '</pre>';
		
		# echo json_encode($meta);
		return $id;
	}
#


function admin_redirect() {
	if ( ( is_single() || is_front_page() || is_page() || is_archive() || is_tax() )
		&& ! is_page( 'minha-conta' ) && ! is_user_logged_in() ) {
		wp_redirect( home_url('/minha-conta/') ); 
		exit;
	} 
} 

add_action('get_header', 'admin_redirect');


/**
 * Function q 
 * source: https://wordpress.stackexchange.com/questions/62889/disable-or-redirect-wp-login-php
 */
add_action('init', 'prevent_wp_login');
function prevent_wp_login() {
    global $pagenow;
	if ($pagenow != 'wp-login.php') {
		return;
	}
    // Check if a $_GET['action'] is set, and if so, load it into $action variable
    $action = (isset($_GET['action'])) ? $_GET['action'] : '';
    // Check if we're on the login page, and ensure the action is not 'logout'
    if( $pagenow == 'wp-login.php' && !current_user_can('administrator') && ( ! $action || ( $action && ! in_array($action, array('logout', 'lostpassword', 'rp', 'resetpass'))))) {
        // Load the home page url
        $page = get_bloginfo('minha-conta');
        // Redirect to the home page
        wp_redirect($page);
        // Stop execution to prevent the page loading for any reason
        exit();
    }
}

function add_publish_confirmation(){ 
    $confirmation_message = "Are you sure you want to publish this post?"; 
 
    echo '<script type="text/javascript">';
    echo '<!-- var publish = document.getElementById("publish");'; 
    echo 'if (publish !== null){';
    echo 'publish.onclick = function(){ return confirm("'.$confirmation_message.'"); };'; 
    echo '}'; 
    echo '// --></script>'; 
} 
# add_action('admin_footer', 'add_publish_confirmation');

do_action( 'wp_head' , function () {
	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    //something posted
    if (isset($_POST['menu-item-2876'])) {
        $page = get_bloginfo('minha-conta');
        // Redirect to the home page
        wp_redirect($page);
		exit();
    } 
}


// RESET BUTTON
function mytheme_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );


function create_facet_reset_btn() {
	?>
	<script>
		console.log("I am here!");
		if ( jQuery( ".fl-col-content.fl-node-content" ).length ) {
			jQuery( ".fl-col-content.fl-node-content" ).append(`<button onclick="FWP.reset()">Reset</button>`);
		}
	</script>
	<?php
}
add_action( 'woocommerce_archive_description', 'create_facet_reset_btn');