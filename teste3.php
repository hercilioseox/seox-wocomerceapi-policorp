<?php
// dados enviados pelo woocommerce
$input = file_get_contents('php://input');
// conversão da string em formato json para array associativo
$inputJson = json_decode($input,true);
// assinatura no header
$allHeaders = getallheaders();
$signature = $allHeaders['X-WC-Webhook-Signature'];
// chave secreta, também escolhida no cadastro do webhook do woocommerce
define ('$VEHMz9{f>qcC<cVKXapi9+wPZ@dlu$(or5[v 0@XmDUWIs(k|', 'wbhook');
 
// arquivo para escrever saída de teste
$fp = fopen('teste.txt', 'w');
if ($fp !== false) {
  // todos os headers
  $str = 'headers: ' . var_export($allHeaders, true) . PHP_EOL;
  // assinatura
  $str .= 'X-WC-Webhook-Signature: ' . $allHeaders['X-WC-Webhook-Signature'] . PHP_EOL;
  // gera assinatura usando o conteúdo e a chave secreta
  $hashHmac = base64_encode(hash_hmac('sha256', $input, SHARED_SECRET, true));
  $str .= 'hashHmac: ' . $hashHmac . PHP_EOL;
  // checa assinatura usando a função definida ali embaixo
  $check = checkSignature($allHeaders['X-WC-Webhook-Signature'], SHARED_SECRET, $input);
  // resultado da checagem
  $str .= 'assinatura ok? ' . var_export($check, true) . PHP_EOL;
  // conteúdo no formato de array associativo, por exemplo $inputJson['product']['title']
  $str .= 'json: ' . var_export($inputJson, true) . PHP_EOL;
  // escreve o texto no arquivo
  fwrite($fp, $str);
  // fecha arquivo
  fclose($fp);
}
 
// função para checar se a assinatura bate, retorna true se bate, false se não.
function checkSignature($webhook_signature, $webhook_key, $content) {
  $signed_data = $content;
  $r = strcmp($webhook_signature, base64_encode(hash_hmac('sha256', $signed_data, $webhook_key, true)));
  return  $r === 0;
}


?>