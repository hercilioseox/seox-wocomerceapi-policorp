var PF_VERSION = "2019-01-30-104840627";

Element.prototype.matches || (Element.prototype.matches = Element.prototype.matchesSelector || Element.prototype.mozMatchesSelector || Element.prototype.msMatchesSelector || Element.prototype.oMatchesSelector || Element.prototype.webkitMatchesSelector || function(e) {
  for (var t = (this.document || this.ownerDocument).querySelectorAll(e), n = t.length; 0 <= --n && t.item(n) !== this; ) ;
  return -1 < n;
}), function() {
  var e = document.getElementById("printfriendly-data");
  if (e) {
    var t = JSON.parse(e.getAttribute("data"));
    window.pfstyle = t.pfstyle, window.pfOptions = t.pfOptions;
  }
  if (window.wrappedJSObject && window.wrappedJSObject.extensionPath) {
    var n = window.wrappedJSObject;
    window.extensionPath = n.extensionPath, window.pfstyle = n.pfstyle, window.pfOptions = n.pfOptions;
  }
}();

var pfMod = window.pfMod || function(a, e) {
  var o = a.document, t = "https:";
  function r(e) {
    coreIframe.contentWindow.postMessage(e, "*");
  }
  a.addEventListener("message", function(e) {
    try {
      if (e.data) {
        var t = e.data.payload;
        switch (e.data.type) {
         case "PfCoreLoaded":
          r({
            type: "PfStartCore",
            payload: {
              pfData: f.pfData
            }
          });
          break;

         case "PfExtensionCoreLoaded":
          r({
            type: "PfLoadCore",
            payload: {
              pfData: f.pfData
            }
          });
          break;

         case "PfClosePreview":
          f.closePreview();
          break;

         case "PfAddCSS":
          l.addCSS(t.css, t.useBody);
          break;

         case "PfRestoreStyles":
          l.restoreStyles();
          break;

         case "PfAddViewPortTag":
          l.addViewPortTag();
          break;

         case "PfScrollTop":
          a.scrollTo(0, 0);
          break;

         case "PfTwitterCopyEmbeded":
          u.copyEmbeded();
          break;

         case "PfCreateByAdType":
          s.createAdByType(t.adType);
          break;

         case "PfShowAds":
          s.show();
          break;

         case "PfHideAds":
          s.hide();
          break;

         case "PfFinished":
          f.hasContent = t.hasContent, f.finished = !0;
          break;

         case "PfRedirectIfNecessary":
          f.dsData = t.dsData;
          var n = f.runRedirectChecks();
          n.redirect ? f.redirect() : (r({
            type: "PfLaunchCore"
          }), c.sendNoRedirectEvent(n.reason));
        }
      }
    } catch (e) {
      y.log(e);
    }
  });
  var s = {
    createAdByType: function(e) {
      if (!document.getElementById("gaiframe")) {
        var t = s.isMobile() ? "_mobile" : "", n = f.config.hosts.cdn + f.config.filePath + f.version + "/ads/" + e + t + ".html", i = document.createElement("iframe");
        i.id = "gaiframe", i.name = "gaiframe", i.style = "border: 0!important; position:absolute!important; height:280px!important; margin-left: auto!important; margin-right: auto!important; left: 0!important; right:0!important; z-index: 2147483647!important; display:none;", 
        i.src = n, i.scrolling = "no", o.body.appendChild(i), s.setupSetStyle();
      }
    },
    isMobile: function() {
      return a.innerWidth <= 700;
    },
    setupSetStyle: function() {
      s.setStyle(), a.addEventListener("resize", function() {
        s.setStyle();
      });
    },
    setStyle: function() {
      var e = document.getElementById("gaiframe");
      if (e) {
        var t = 860 < a.innerWidth ? "284px" : "250px", n = 730 < a.innerWidth ? "700px" : a.innerWidth;
        e.style.removeProperty("width"), e.style.removeProperty("top"), e.style.setProperty("width", n, "important"), 
        e.style.setProperty("top", t, "important");
      }
    },
    show: function() {
      var e = document.getElementById("gaiframe");
      e && (e.style.display = "block");
    },
    hide: function() {
      var e = document.getElementById("gaiframe");
      e && (e.style.display = "none");
    }
  }, c = {
    sendEvent: function(e, t, n) {
      r({
        type: "PfGaEvent",
        payload: {
          category: e,
          action: t,
          label: n
        }
      });
    },
    sendNoRedirectEvent: function(e) {
      var t = f.config.isExtension ? "extension" : "button";
      this.sendEvent("NoRedirect", t, e);
    }
  }, n = {
    environment: "production",
    disableUI: !1,
    protocol: t,
    dir: "ltr",
    usingBM: !1,
    maxImageWidth: 750,
    filePath: "/assets/versions/",
    platform: "unknown",
    hosts: {
      cdn: t + "//cdn.printfriendly.com",
      pf: "https://www.printfriendly.com",
      ds: "https://www.printfriendly.com",
      translations: "https://www.printfriendly.com",
      ds_cdn: "https://ds-4047.kxcdn.com",
      pdf: "https://pdf.printfriendly.com",
      email: "https://www.printfriendly.com",
      page: a.location.host.split(":")[0],
      ravenDsn: "https://5463b49718cd4266911eab9e5c0e114d@sentry.io/22091"
    },
    domains: {
      page: a.location.host.split(":")[0].split("www.").pop()
    }
  }, d = {
    isBookmarklet: function() {
      return a.pfstyle && "wp" != a.pfstyle;
    },
    removeEmailsFromUrl: function(e) {
      for (var t = (e = e.split("?")[0]).split("/"), n = t.length; 0 < n--; ) -1 !== t[n].indexOf("@") && t.splice(n, 1);
      return t.join("/");
    },
    isDynamicPage: function() {
      return !!(a.React || a.ko || a.Polymer || a.m || a.angular || a.ng && a.ng.coreTokens || a.Backbone || a.Ember || a.Vue || document.querySelector && document.querySelector('[ng-version],[data-reactroot],[data-bind],[class*="svelte-"],.__meteor-css__'));
    },
    ogImageUrl: function() {
      var e = "", t = document.querySelector && document.querySelector('meta[property="og:image"]');
      return t && t.content && (e = t.content), e;
    },
    isWix: function() {
      return -1 !== d.ogImageUrl().indexOf("wixstatic.com");
    },
    isOverBlog: function() {
      return -1 !== d.ogImageUrl().indexOf("over-blog-kiwi.com");
    },
    isLocalHost: function() {
      var e = a.location.host, t = a.location.hostname;
      return -1 !== e.indexOf(":") || !!t.match(/\d+\.\d+\.\d+\.\d+/) || "localhost" === t || !!t.split(".").pop().match(/invalid|test|example|localhost|dev/);
    }
  }, l = {
    addViewPortTag: function() {
      var e = o.getElementsByTagName("head")[0], t = o.querySelector("meta[name=viewport]");
      t || ((t = o.createElement("meta")).name = "viewport"), t.content = "width=device-width, initial-scale=1", 
      e.appendChild(t);
    },
    addCSS: function(e, t) {
      var n = t ? "body" : "head", i = o.getElementsByTagName(n)[0], r = o.createElement("style");
      r.type = "text/css", r.setAttribute("name", "pf-style"), r.styleSheet ? r.styleSheet.cssText = e : r.appendChild(o.createTextNode(e)), 
      i.appendChild(r);
    },
    restoreStyles: function() {
      for (var e = document.getElementsByName("pf-style"), t = e.length - 1; 0 <= t; t--) e[t].parentNode.removeChild(e[t]);
    }
  }, m = {
    isReady: !1,
    readyBound: !1,
    ready: function() {
      if (!m.isReady) {
        if (!document.body) return setTimeout(m.ready, 13);
        m.isReady = !0, m.readyFunc.call();
      }
    },
    doScrollCheck: function() {
      if (!m.isReady) {
        try {
          document.documentElement.doScroll("left");
        } catch (e) {
          return setTimeout(m.doScrollCheck, 50);
        }
        m.detach(), m.ready();
      }
    },
    detach: function() {
      document.addEventListener ? (document.removeEventListener("DOMContentLoaded", m.completed, !1), 
      a.removeEventListener("load", m.completed, !1)) : document.attachEvent && "complete" === document.readyState && (document.detachEvent("onreadystatechange", m.completed), 
      a.detachEvent("onload", m.completed));
    },
    completed: function(e) {
      (document.addEventListener || "load" === e.type || "complete" === document.readyState) && (m.detach(), 
      m.ready());
    },
    bindReady: function() {
      if (!m.readyBound) {
        if (m.readyBound = !0, "complete" === document.readyState) return m.ready();
        if (document.addEventListener) document.addEventListener("DOMContentLoaded", m.completed, !1), 
        a.addEventListener("load", m.completed, !1); else if (document.attachEvent) {
          document.attachEvent("onreadystatechange", m.completed), a.attachEvent("onload", m.completed);
          var e = !1;
          try {
            e = null == a.frameElement && document.documentElement;
          } catch (e) {}
          e && e.doScroll && m.doScrollCheck();
        }
      }
    },
    domReady: function(e) {
      this.readyFunc = e, this.bindReady();
    },
    canonicalize: function(e) {
      if (e) {
        var t = document.createElement("div");
        return t.innerHTML = "<a></a>", t.firstChild.href = e, t.innerHTML = t.innerHTML, 
        t.firstChild.href;
      }
      return e;
    }
  }, p = {
    headerImageUrl: m.canonicalize(a.pfHeaderImgUrl),
    headerTagline: a.pfHeaderTagline,
    imageDisplayStyle: a.pfImageDisplayStyle,
    customCSSURL: m.canonicalize(a.pfCustomCSS),
    disableClickToDel: a.pfdisableClickToDel,
    disablePDF: a.pfDisablePDF,
    disablePrint: a.pfDisablePrint,
    disableEmail: a.pfDisableEmail
  };
  -1 !== "|full-size|remove-images|large|medium|small|".indexOf("|" + a.pfImagesSize + "|") ? p.imagesSize = a.pfImagesSize : p.imagesSize = 1 == a.pfHideImages ? "remove-images" : "full-size";
  var f = {
    version: PF_VERSION,
    initialized: !1,
    finished: !1,
    onServer: !1,
    hasContent: !1,
    messages: [],
    errors: [],
    waitDsCounter: 0,
    autoStart: !1,
    stats: {},
    init: function(e) {
      try {
        this.initialized = !0, this.configure(e), this.onServerSetup(), f.onServer || this.config.isExtension || this.getAdSettingsFromPFServer(), 
        this.setVariables(), this.detectBrowser(), this.setStats(), this.startIfNecessary(), 
        a.print = function() {
          f.start();
        };
      } catch (e) {
        y.log(e);
      }
    },
    configure: function(e) {
      if (this.config = n, e) {
        for (var t in this.config.environment = e.environment || "development", e.hosts) this.config.hosts[t] = e.hosts[t];
        e.filePath && (this.config.filePath = e.filePath), e.ssLocation && (this.config.ssLocation = e.ssLocation), 
        e.ssStyleSheetHrefs && (this.config.ssStyleSheetHrefs = e.ssStyleSheetHrefs);
      }
      this.config.isExtension = !!a.extensionPath;
    },
    getVal: function(e, t) {
      for (var n = t.split("."); void 0 !== e && n.length; ) e = e[n.shift()];
      return e;
    },
    startIfNecessary: function() {
      (a.pfstyle || this.autoStart) && this.start();
    },
    urlHasAutoStartParams: function() {
      return -1 !== this.config.urls.page.indexOf("pfstyle=wp");
    },
    start: function() {
      if (f.onServer || f.config.isExtension) f.launch(); else {
        if (f.waitDsCounter += 1, f.waitDsCounter < 20 && !f.dsData) return setTimeout(function() {
          f.start();
        }, 100);
        var e;
        f.runRedirectChecks().redirect ? f.redirect() : f.supportedSiteType() && f.launch();
      }
    },
    launch: function() {
      m.domReady(function() {
        try {
          f.startTime = new Date().getTime(), g.run(), f.pfData = h.get(), f.config.disableUI || (f.sanitizeLocation(), 
          f.createMask()), f.loadCore();
        } catch (e) {
          y.log(e);
        }
      });
    },
    sanitizeLocation: function() {
      url = document.location.href.split("?")[0], url = d.removeEmailsFromUrl(url), url !== document.location.href && (history && "function" == typeof history.pushState ? history.pushState({
        pf: "sanitized"
      }, document.title, url) : f.urlHasPII = !0);
    },
    unsanitizeLocation: function() {
      history && history.state && "sanitized" == history.state.pf && history.back();
    },
    onServerSetup: function() {
      a.onServer && (this.onServer = !0, this.config.disableUI = !0);
    },
    setVariables: function() {
      var e = this, t, n = "";
      "production" !== this.config.environment && (n = "?_=" + Math.random());
      var i = e.config.hosts.cdn + e.config.filePath + e.version, r = this.config.disableUI ? "" : i + "/pf-app/main.css" + n, o = this.config.disableUI ? "" : i + "/content/main.css" + n;
      this.config.urls = {
        version: i,
        js: {
          jquery: "https://cdn.printfriendly.com/assets/unversioned/jquery/1.12.4.min.js",
          raven: "https://cdn.ravenjs.com/3.19.1/raven.min.js",
          core: i + "/core.js" + n,
          algo: i + "/algo.js" + n
        },
        css: {
          pfApp: r,
          content: o
        },
        pdfMake: e.config.hosts.pdf + "/pdfs/make",
        email: e.config.hosts.email + "/email/new"
      };
      try {
        t = top.location.href;
      } catch (e) {
        t = a.location.href;
      }
      this.config.urls.page = t, this.userSettings = p, this.config.pfstyle = a.pfstyle, 
      !a.pfstyle || "bk" !== a.pfstyle && "nbk" !== a.pfstyle && "cbk" !== a.pfstyle || (this.config.usingBM = !0), 
      this.autoStart = this.urlHasAutoStartParams();
    },
    detectBrowser: function() {
      this.browser = {};
      var e = navigator.appVersion;
      e && -1 !== e.indexOf("MSIE") ? (this.browser.version = parseFloat(e.split("MSIE")[1]), 
      this.browser.isIE = !0) : this.browser.isIE = !1;
    },
    loadScript: function(e, t) {
      var n = document.getElementsByTagName("head")[0], i = document.createElement("script");
      i.type = "text/javascript", i.src = e, i.onreadystatechange = t, i.onload = t, n.appendChild(i);
    },
    createIframe: function(e) {
      var t = e.createElement("iframe");
      return t.frameBorder = "0", t.allowTransparency = "true", t;
    },
    loadHtmlInIframe: function(t, n, e) {
      var i, r;
      try {
        r = n.contentWindow.document;
      } catch (e) {
        i = t.domain, r = n.contentWindow.document;
      }
      r.write(e), r.close();
    },
    redirect: function() {
      var e = [ "source=cs", "url=" + encodeURIComponent(top.location.href) ];
      for (var t in p) void 0 !== p[t] && e.push(t + "=" + encodeURIComponent(p[t]));
      var n = this.config.hosts.pf + "/print/?" + e.join("&");
      this.autoStart ? top.location.replace(n) : top.location.href = n;
    },
    supportedSiteType: function() {
      return "about:blank" !== f.config.urls.page && ("http:" === f.config.protocol || "https:" === f.config.protocol);
    },
    setStats: function() {
      f.stats.page = {
        bm: d.isBookmarklet(),
        dynamic: d.isDynamicPage(),
        local: d.isLocalHost(),
        unSupported: d.isWix() || d.isOverBlog()
      };
    },
    skipRedirectReasons: [ {
      name: "adFree",
      check: function() {
        return f.dsData && f.dsData.domain_settings.ad_free;
      }
    }, {
      name: "isDynamicPage",
      check: function() {
        return f.dsData && d.isDynamicPage();
      }
    }, {
      name: "isLocalHost",
      check: function() {
        return f.dsData && d.isLocalHost();
      }
    }, {
      name: "unSupportedBySS",
      check: function() {
        return f.dsData && (d.isWix() || d.isOverBlog());
      }
    }, {
      name: "notButtonRedirectNorBookmarkletRedirect",
      check: function() {
        var e = d.isBookmarklet();
        if (f.dsData) return !(!e && f.dsData.domain_settings.redirect || e && f.dsData.domain_settings.bk_redirect);
      }
    } ],
    forceRedirectReasons: [ {
      name: "unSupportedBrowser",
      check: function() {
        try {
          var e = navigator.userAgent.match(/Edge\/(\d+.\d+)/);
          return !!(!history || "function" != typeof history.pushState || navigator.userAgent.match(/(ipod|opera.mini|blackberry|playbook|bb10)/i) || f.browser.isIE && f.browser.version < 11 || f.browser.isIE && a.adsbygoogle || "undefined" != typeof $ && $.jcarousel && f.browser.isIE || e && e[1] && parseFloat(e[1]) < 13.10586);
        } catch (e) {
          return y.log(e), !1;
        }
      }
    } ],
    runChecksFor: function(e) {
      for (var t = 0, n = e.length; t < n; t++) {
        var i = e[t];
        if (i.check()) return i;
      }
    },
    runRedirectChecks: function() {
      var e = this.runChecksFor(this.skipRedirectReasons);
      if (!e) return {
        redirect: !0
      };
      var t = this.runChecksFor(this.forceRedirectReasons);
      return t ? {
        reason: t.name,
        redirect: !0
      } : {
        reason: e.name,
        redirect: !1
      };
    },
    createMask: function() {
      var e = document.createElement("div");
      e.innerHTML = '<div id="pf-mask" style="z-index: 2147483627!important; position: fixed !important; top: 0pt !important; left: 0pt !important; background-color: rgb(0, 0, 0) !important; opacity: 0.8 !important;filter:progid:DXImageTransform.Microsoft.Alpha(opacity=80); height: 100% !important; width: 100% !important;"></div>', 
      document.body.appendChild(e.firstChild);
    },
    closePreview: function() {
      m.readyBound = !1, m.isReady = !1, f.unsanitizeLocation();
      var e = document.getElementById("pf-core");
      e && e.parentElement && e.parentElement.removeChild(e);
      var t = document.getElementById("pf-mask");
      t && t.parentElement && t.parentElement.removeChild(t);
      var n = document.getElementById("gaiframe");
      n && n.parentElement && n.parentElement.removeChild(n);
    },
    removeDoubleSemiColon: function(e) {
      return e.replace(/;{2}/g, ";");
    },
    loadCore: function() {
      a.coreIframe = this.createIframe(document), coreIframe.id = "pf-core", coreIframe.name = "pf-core", 
      document.body.appendChild(coreIframe);
      var e = coreIframe.style.cssText + ";width: 100% !important;max-width:100% !important;height: 100% !important; display: block !important; overflow: hidden !important; position: absolute !important; top: 0px !important; left: 0px !important; background-color: transparent !important; z-index: 2147483647!important";
      if (coreIframe.style.cssText = this.removeDoubleSemiColon(e), this.config.isExtension) coreIframe.src = extensionPath + "/core.html"; else {
        var t = '<!DOCTYPE html><html><head><base target="_parent"><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1"><script src="' + this.config.urls.js.jquery + '"><\/script>';
        f.onServer || (t += '<script src="' + this.config.urls.js.raven + '"><\/script>'), 
        t += '<script src="' + this.config.urls.js.core + '"><\/script><link media="screen" type="text/css" rel="stylesheet" href="' + this.config.urls.css.pfApp + '"></head><body class="cs-core-iframe" onload="core.init();"></body>', 
        this.loadHtmlInIframe(document, coreIframe, t);
      }
    },
    getAdSettingsFromPFServer: function() {
      var e = document.createElement("script");
      e.src = f.config.hosts.ds_cdn + "/api/v3/domain_settings/a?callback=pfMod.saveAdSettings&hostname=" + f.config.hosts.page + "&client_version=" + f.version, 
      document.getElementsByTagName("head")[0].appendChild(e);
    },
    saveAdSettings: function(e) {
      f.dsData = e, a.coreIframe && coreIframe.contentWindow && r({
        type: "PfConfigureAdSettings",
        payload: {
          dsData: e
        }
      });
    }
  }, h = {
    emailText: function() {
      var e = document.getElementsByClassName("pf-email");
      return e.length ? e[0].textContent : "";
    },
    csStyleSheetHrefs: function() {
      var e = [];
      for (i = 0; i < o.styleSheets.length; i++) e.push(o.styleSheets[i].href);
      return e;
    },
    metas: function() {
      var e = o.getElementsByTagName("meta"), t = [];
      for (i = 0; i < e.length; i++) t.push({
        name: e[i].getAttribute("name"),
        content: e[i].getAttribute("content"),
        property: e[i].getAttribute("property"),
        itemprop: e[i].getAttribute("itemprop")
      });
      return t;
    },
    screen: function() {
      return {
        x: void 0 !== a.top.screenX ? a.top.screenX : a.top.screenLeft,
        y: void 0 !== a.top.screenY ? a.top.screenY : a.top.screenTop,
        width: void 0 !== a.top.outerWidth ? a.top.outerWidth : a.top.document.documentElement.clientWidth,
        height: void 0 !== a.top.outerHeight ? a.top.outerHeight : a.top.document.documentElement.clientHeight - 22
      };
    },
    language: function() {
      var e = document.getElementsByTagName("html")[0].getAttribute("lang");
      if (!e) {
        var t = document.querySelector("meta[http-equiv=Content-Language]");
        t && (e = t.getAttribute("content"));
      }
      return e;
    },
    canvasDataUrls: function() {
      for (var e = [], t = o.getElementsByTagName("canvas"), n = 0; n < t.length; n++) try {
        var i = t[n], r = i.toDataURL("image/png");
        i.setAttribute("pf-dataurl-index", e.length), e.push(r);
      } catch (e) {}
      return e;
    },
    favicon: function() {
      for (var e, t = document.getElementsByTagName("link"), n = 0; n < t.length; n++) {
        var i = t[n], r = i.getAttribute("rel");
        if ("icon" === r || "shortcut icon" === r) {
          e = i.getAttribute("href");
          break;
        }
      }
      return 0 !== (e = e || "favicon.ico").indexOf("http") && (e = a.location.protocol + "//" + a.location.host + "/" + e), 
      e;
    },
    get: function() {
      f.config.extensionPath = a.extensionPath;
      var e = this.canvasDataUrls(), t = document.location;
      return page = {
        dir: o.body.getAttribute("dir") || o.querySelector("html").getAttribute("dir") || getComputedStyle(o.body).getPropertyValue("direction") || "ltr",
        bodyClassList: [].slice.call(o.body.classList),
        emailText: this.emailText(),
        screen: this.screen(),
        metas: this.metas(),
        csStyleSheetHrefs: this.csStyleSheetHrefs(),
        location: {
          href: t.href,
          host: t.host,
          pathname: t.pathname,
          protocol: t.protocol
        },
        hasPrintOnly: 0 !== o.querySelectorAll("#print-only, .print-only").length,
        title: document.title,
        body: document.body.innerHTML,
        language: this.language(),
        canvasDataUrls: e,
        favicon: this.favicon()
      }, {
        startTime: f.startTime,
        config: f.config,
        userSettings: f.userSettings,
        version: f.version,
        onServer: f.onServer,
        browser: f.browser,
        urlHasPII: f.urlHasPII,
        dsData: f.dsData,
        stats: f.stats,
        page: page
      };
    }
  }, u = {
    copyEmbeded: function() {
      var e, t, n;
      for (n = (e = o.querySelectorAll("twitter-widget.twitter-tweet-rendered")).length - 1; 0 <= n; n--) r({
        type: "PfTwitterWidgetShadowDom",
        payload: {
          id: (t = e[n]).id,
          innerHTML: t.shadowRoot.innerHTML,
          cssText: t.style.cssText
        }
      });
      for (n = (e = o.querySelectorAll("iframe.twitter-tweet-rendered")).length - 1; 0 <= n; n--) e = e[n], 
      r({
        type: "PfTwitterTweetRendered",
        payload: {
          id: t.id,
          head: t.contentDocument.head.innerHTML,
          body: t.contentDocument.body.innerHTML,
          cssText: t.style.cssText
        }
      });
    }
  }, g = {
    LARGE_IMAGE_WIDTH: 300,
    LARGE_IMAGE_HEIGHT: 200,
    LAZY_MATCHERS: 'img[original], img[data-lazy-src], img[data-href], img[data-src], img[data-pagespeed-lazy-src], img[data-original], img.lazyload, img[data-mediaviewer-src], img[datasrc], img[data-native-src], img[itemprop="url"], img.js-progressiveMedia-image, img[data-original-src], a[data-replace-image]',
    run: function() {
      this.processChildren(document.body);
    },
    processChildren: function(e) {
      for (var t, n, i = e.firstChild; i; ) {
        if (!i.classList || !i.classList.contains("comment-list")) {
          if (i.nodeType === Node.ELEMENT_NODE) try {
            t = i.nodeName.toLowerCase(), "none" === (n = i.currentStyle || a.getComputedStyle(i)).display || "hidden" === n.visibility ? i.matches(this.LAZY_MATCHERS) || i.classList.add("hidden-originally") : i.classList.contains("hidden-originally") && i.classList.remove("hidden-originally"), 
            "a" === t ? (href = i.getAttribute("href"), href && "#" !== href.charAt(0) && (i.href = i.href)) : "input" !== t && "textarea" !== t || pfMod.onServer ? "select" !== t || pfMod.onServer ? "img" !== t && "svg" !== t || (i.src = i.src, 
            pfMod.onServer || (!i.getAttribute("width") && i.clientWidth && i.setAttribute("width", i.clientWidth), 
            !i.getAttribute("height") && i.clientHeight && i.setAttribute("height", i.clientHeight)), 
            "img" === t && !i.classList.contains("hidden-originally") && i.clientWidth > this.LARGE_IMAGE_WIDTH && i.clientHeight > this.LARGE_IMAGE_HEIGHT && i.classList.add("pf-large-image")) : i.options[i.selectedIndex].setAttribute("selected", "selected") : "radio" === i.type || "checkbox" === i.type ? i.checked && i.setAttribute("checked", "checked") : i.setAttribute("value", i.value);
          } catch (e) {}
          i.hasChildNodes() && this.processChildren(i);
        }
        i = i.nextSibling;
      }
    }
  }, y = {
    _window: a.top,
    _doc: a.top.document,
    installInitiated: !1,
    validFile: /d3nekkt1lmmhms|printfriendly\.com|printnicer\.com|algo\.js|printfriendly\.js|core\.js/,
    setVars: function() {
      this._window.frames["pf-core"] && this._window.frames["pf-core"].document && (this._window = this._window.frames["pf-core"], 
      this._doc = this._window.document);
    },
    install: function() {
      if (this.installInitiated) return !0;
      this.installInitiated = !0, this.setVars();
      var e = this._doc.createElement("script"), t = this._doc.getElementsByTagName("script")[0];
      e.src = this.config.urls.js.raven, t.parentNode.appendChild(e), this.wait();
    },
    wait: function() {
      if (!this._window.Raven) return setTimeout(function() {
        y.wait();
      }, 100);
      this.configure(), this.pushExistingErrors();
    },
    configure: function() {
      var e = {
        dataCallback: function(e) {
          var t, n;
          try {
            (t = e.stacktrace.frames[0]).filename.match(y.validFile) && "onload" !== t.function || e.stacktrace.frames.shift();
          } catch (e) {}
          return e;
        },
        shouldSendCallback: function(e) {
          return !!(e && e.extra && e.extra.file);
        },
        release: f.version
      };
      this._window.Raven.config(f.config.hosts.ravenDsn, e).install();
    },
    sendError: function(e, t) {
      (t = void 0 !== t ? {
        file: t.file
      } : {
        file: "printfriendly.js"
      }).usingBM = f.config.usingBM, t.url = f.config.urls.page, "production" === f.config.environment && this._window.Raven.captureException(e, {
        extra: t
      });
    },
    pushExistingErrors: function() {
      for (var e = 0; e < f.errors.length; e++) this.sendError(f.errors[e].err, f.errors[e].opts);
    },
    log: function(e, t) {
      "development" === f.config.environment && console.error(e), f.finished = !0, t = t || {
        file: "printfriendly.js"
      };
      try {
        this._window.Raven ? this.sendError(e, t) : (f.errors.push({
          err: e,
          opts: t
        }), this.install(), f.messages.push(e.name + " : " + e.message), f.messages.push(e.stack));
      } catch (e) {}
    }
  };
  return f.exTracker = y, f;
}(window), priFri = pfMod;

pfMod.initialized && (document.getElementById("printfriendly-data") || window.extensionPath) ? pfMod.start() : "algo" === window.name || "pf-core" === window.name || pfMod.initialized || pfMod.init(window.pfOptions);