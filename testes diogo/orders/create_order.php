<?php 
include '../../conex.php';

$data = [
    'payment_method' => 'CC',
    'payment_method_title' => 'Credit Card',
    'set_paid' => true,
    'billing' => [
        'first_name' => 'Hercilio',
        'last_name' => 'Ortiz',
        'address_1' => '969 Market',
        'address_2' => '',
        'city' => 'San Francisco',
        'state' => 'CA',
        'postcode' => '94103',
        'country' => 'US',
        'email' => 'john.doe@example.com',
        'phone' => '(555) 555-5555'
    ],
    'shipping' => [
        'first_name' => 'Hercilio',
        'last_name' => 'Ortiz',
        'address_1' => '969 Market',
        'address_2' => '',
        'city' => 'San Francisco',
        'state' => 'CA',
        'postcode' => '94103',
        'country' => 'US'
    ],
    'line_items' => [
        [
            'product_id' => 48,
            'quantity' => 1
        ],
        [
            'product_id' => 62,
            'quantity' => 1
        ]
    ]
];
echo '<pre>';
print_r($woocommerce->post('orders', $data));

?>