<?php 
try {
    $root_att = Buscar_id_wooAPI('products', $prod_id)->attributes;
    if (count($root_att) <= 0) {
        $root_att = [
            0 => [
                'name'      => 'Cor / Tecido',
                'visible'   => true, 
                'variation' => true,
                'options'   => [$variacao[0]['NOME']]
            ],
            1 => [
                'name'      => 'Tamanho',
                'visible'   => true, 
                'variation' => true,
                'options'   => [$variacao[0]['TAMANHO']]
            ]
        ];
    } else if ($root_att[0]->name === 'Tamanho' ) {
        if (!in_array($variacao[0]['TAMANHO'], $root_att[0]->options)) {
            $root_att[0]->options[] = $variacao[0]['TAMANHO'];
        }
        if (count($root_att) == 1) {
            $root_att[] = [
                'name'      => 'Cor / Tecido',
                'visible'   => true, 
                'variation' => true,
                'options'   => [$variacao[0]['NOME']]
            ];
        } else if (!in_array($variacao[0]['NOME'], $root_att[1]->options)) {
            $root_att[1]->options[] = $variacao[0]['NOME'];
        }
    } else {
        if (!in_array($variacao[0]['NOME'], $root_att[0]->options)) {
            $root_att[0]->options[] = $variacao[0]['NOME'];
        }
        if (count($root_att) == 1) {
            $root_att[] = [
                'name'      => 'TAMANHO',
                'visible'   => true, 
                'variation' => true,
                'options'   => [$variacao[0]['TAMANHO']]
            ];
        } else if (!in_array($variacao[0]['TAMANHO'], $root_att[1]->options)) {
            $root_att[1]->options[] = $variacao[0]['TAMANHO'];
        }
    } 
    $array_att = [
        json_decode(json_encode($root_att[0]), True)
        , json_decode(json_encode($root_att[1]), True)
    ];
        
    // Tamanho
    Atualizar_atributos_em_um_produto_wooAPI($prod_id, $array_att);
} catch (Exception $e) {
    Logging($funcao='Inserir_atualizar_variacao_registrada_wooAPI', $status='erro: erro ao tentar atualizar atributos.');
    return;
}
?>