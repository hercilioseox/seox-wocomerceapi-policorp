<?php 
// Esto só funciona para atributos globais.
function Inserir_uma_variacao_para_cada_atributo_wooAPI($id_produto) {
    $adress_insert = 'products/'.$id_produto.'/variations/batch';
    $data = [
        'create' => [
            [
                'attributes' => []
            ]
        ]
    ];
    // Armazena atributos de um produto
    $att = Buscar_id_wooAPI('products', $id_produto)->attributes;
    // Variavel utilizada para navegar entre os atributos
    $i = 0;
    // Auxilia na criacao do array $data
    $index_batch = 0;
    $parents = [];

    foreach($att[$i]->options as $op) {
        if ($i+1 < count($att)) {
            $parents[$i] = [
                'id' => $att[$i]->id,
                'op' => $op
            ];
            $data = Inserir_uma_variacao_para_cada_atributo_wooAPI_auxiliar($att, $data, $i+1, $parent=$parents, $index_batch);
            $index_batch = count($data['create']);
            $parents = [];
        } else {
            // Caso haja somente um tipo de atributo, ele cria uma variacao para cada
            $data['create'][$index_batch]['attributes'][] = [
                'id' => $att[$i]->id,
                'option' => $op
            ];
            $index_batch++;
        }
    }
    // echo '<pre>';
    // print_r($data);
    Inserir_wooAPI($adress_insert, $data);
    return Buscar_produto_variacoes_wooAPI($id_produto);
}

function Inserir_uma_variacao_para_cada_atributo_wooAPI_auxiliar($att, $data, $index, $parent, $index_batch) {
    foreach($att[$index]->options as $opcao) {

        if($index == count($att)-1) {
            for($i=0; $i<count($parent); $i++) {
                // Adiciona todos os elementos percorridos ao $data
                $data['create'][$index_batch]['attributes'][] = [
                    'id' => $parent[$i]['id'],
                    'option' => $parent[$i]['op']
                ]; 
            }
            // Adiciona o ultimo elemento ao $data
            $data['create'][$index_batch]['attributes'][] = [
                'id' => $att[$index]->id,
                'option' => $opcao
            ]; 
            $index_batch++;
        }
        // Ele soh chama outra recursao se houver mais atributos para calcular.
        else {
            $parent[$index] = [
                'id' => $att[$index]->id,
                'op' => $opcao
            ];
            // Obs: se eu mandar o $i+1 como $index=$i+1 ele atualiza o index globalmente
            $data = Inserir_uma_variacao_para_cada_atributo_wooAPI_auxiliar($att, $data, $index+1, $parent, $index_batch);
            $index_batch = count($data['create']);
        }
    }
    return $data; 
}

?>