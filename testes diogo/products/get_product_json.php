
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<?php 
include '../../conex.php';

$products = $woocommerce->get('products');
?>
<div class="container">
<h2 class="sub-header">Products List</h2>
<div class='table-responsive'>
<table id='myTable' class='table table-striped table-bordered'>
<thead>
    <tr>
        <th>SKU</th>
        <th>Name</th>
        <th>Status</th>
        <th>Price</th>
        <th>Total Sales</th>
        <th>Picture</th>
    </tr>
</thead>
<tbody>
<?php
foreach($products as $product){
    echo "<tr><td>" . $product->sku."</td>

    <td>" . $product->name."</td>

    <td>" . $product->status."</td>

    <td>" . $product->price."</td>

    <td>" . $product->total_sales."</td>";

    if (count($product->images) > 0 ) {
        echo "<td><img height='50px' width='50px' src='".$product->images[0]->src."'></td></tr>";
    } else {
        echo "<td><img height='50px' width='50px'></td></tr>";
    }
}
?>
</tbody>
</table>
</div>
</div>
</body>
</html>

