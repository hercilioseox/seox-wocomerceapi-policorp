<?php
// source: http://docs.guzzlephp.org/en/stable/quickstart.html#making-a-request
// https://woocommerce-236437-737460.cloudwaysapps.com/wp-json/wp/v2/product
// instalar dependencias: composer require guzzlehttp/guzzle
// para acessar a resposta em json utilizar funcao: json_decode()
// how to genarate the tokens: https://www.youtube.com/watch?v=oJjYuZgySig
// how to use the tokens: https://github.com/guzzle/oauth-subscriber
// POST/GET/PUT in guzzle: https://hackernoon.com/creating-rest-api-in-php-using-guzzle-d6a890499b02

require __DIR__ . '/vendor/autoload.php';

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Oauth\Oauth1;

$stack = HandlerStack::create();

$middleware = new Oauth1([
    // woocommerce teste keys:
    // 'consumer_key'      => 'zIrRdfj2tBCb'
    // , 'consumer_secret' => 'Pe8y1m8PoOmygyyCS74XlnBqb5CyjPiS0NfBoqlh77IgKbbe'
    // , 'token'           => 'Rh84wdYYW55UbGjzQJ6ZvDje'
    // , 'token_secret'    => 'XTPulmq5jqWOt43Edg9OS6AevnN4Dot1g9X4cYChkiriz8xg'

    // POLICORP OFICIAL keys:
    'consumer_key'      => 'Rb9IsexJUN2l'
    , 'consumer_secret' => '4GhqzTAhvYX2JFWkzLGoBjm9CpHyabuwyunSKJsR0Q5gQF12'
    , 'token'           => '888DR7zkiVNxyIFKFEM8G1SN'
    , 'token_secret'    => 'GM4oGbPH2DbzFIQNpqZtEsP45kRLATJFmVRlhEt5C2YTpFwW'
]);

$stack->push($middleware);

$wordpress = new Client([
    // wocommerce teste url:
    // 'base_uri'  => 'https://woocommerce-236437-737460.cloudwaysapps.com' 
    // POLICORP OFICIAL URL:
    'base_uri'  => 'http://policorp.seox.com.br' 
    ,'timeout'  => 3600
    , 'handler' => $stack
    , 'verify'  => false
    , 'auth'    => 'oauth'
]);

?>
