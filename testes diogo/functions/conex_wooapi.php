<?php 

//https://www.cloudways.com/blog/custom-dashboard-using-woocommerce-php-rest-api/
//	https://packagist.org/packages/automattic/woocommerce
require __DIR__ . '/vendor/autoload.php';

use Automattic\WooCommerce\Client;

try {
	$woocommerce = new Client(
		//POLICORP OFICIAL
		'http://policorp.seox.com.br'							 // - Projeto real
		, 'ck_9914f6dbae3434d10d983618c63b70388f24f3c3'          // - Projeto real
		, 'cs_8b77204efa3263158643741615990bb43bd3d224'		     // - Projeto real
		// woocommerce teste
		// 'https://woocommerce-236437-737460.cloudwaysapps.com' // - Projeto teste
		// , 'ck_33296aee302a15399c2f0cbb2a9d2eb3e3daecd4'        // - Projeto teste
		// , 'cs_08ff158e8e08277ec429a87b5152d6a1d8aea32d'       // - Projeto teste
		, [
			'version'      => 'wc/v3'
			, 'verify_ssl' => false 
			, 'timeout'    => 3600
		]
	);
} catch (Exception $e) {
	echo '<pre>';
	print_r($e);
}


use Automattic\WooCommerce\HttpClient\HttpClientException;

?>