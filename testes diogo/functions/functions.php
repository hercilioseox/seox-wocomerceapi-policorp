<?php

/***************************
 * FUNCOES API WOOCOMMERCE *
 ***************************/

/** 
 * CRUD
 * 
 * Discionario para parametro $tabela:
 * - Usuarios     => customers
 * - Produtos     => products
 * - Categorias   => products/categories
 * - Pedidos      => orders
 */
function Inserir_wooAPI($address, $data) {
    include 'conex_wooapi.php';

    try {
        $result = $woocommerce->post($address, $data);
        return $result;
    } catch (Exception $e) {
        Logging($funcao='Inserir_wooAPI : '.$address, $status=$e);
    }
}

function Atualizar_wooAPI($address, $data) {
    include 'conex_wooapi.php';

    try {
        $result = $woocommerce->put($address, $data);
        return $result;
    } catch (Exception $e) {
        Logging($funcao='Atualizar_wooAPI : '.$address, $status=$e);
    }
}

function Buscar_wooAPI($address) {
    include 'conex_wooapi.php';

    try {
        $result = $woocommerce->get($address);
        return $result;
    } catch (Exception $e) {
        Logging($funcao='Buscar_wooAPI : '.$address, $status='erro');
    }
}

function Buscar_id_wooAPI($address, $id) {
    include 'conex_wooapi.php';

    try {
        $result = $woocommerce->get($address.'/'.$id);
        return $result;
    } catch (Exception $e) {
        Logging($funcao='Buscar_id_wooAPI : '.$address, $status='erro');
    }
}

function Verificar_wooAPI($address, $id) {
    include 'conex_wooapi.php';
    
    try {
        $result = $woocommerce->get($address.'/'.$id);
        if ($result != null) {
            return true; 
        }
        return false;
    } catch (Exception $e) {
        return false;
    } 
}

function Deletar_wooAPI($address) {
    include 'conex_wooapi.php';

    try {
        $result = $woocommerce->delete($address, ['force' => true]);
        return $result;
    } catch (Exception $e) {
        Logging($funcao='Deletar_wooAPI : '.$address, $status=$e);
    }
}

function Batch_wooAPI($address, $data) {
    set_time_limit(0);
    include 'conex_wooapi.php';

    try {
        $result = $woocommerce->post($address.'/batch', $data);
        return $result;
    } catch (Exception $e) {
        Logging($funcao='Batch_wooAPI : '.$address, $status=$e);
    }
}

/**
 * GET id com base no sku
 */
function Buscar_id_pelo_sku_wooAPI(string $sku) {
    $prod = Buscar_produto_sku_wooAPI($sku);
    return $prod[0]->id;
}

/**
 * GET produto com base no sku
 */
function Buscar_produto_sku_wooAPI(string $sku) {
    include 'conex_wooapi.php';
    
    try {
        $result = $woocommerce->get('products', array('sku' => $sku));
        return $result;
    } catch (Exception $e) {
        Logging($funcao='Buscar_produto_sku_wooAPI', $status='erro');
    } 
}

/**
 * GET cliente com base no meta data
 */
function Buscar_cliente_meta_wooAPI($id_caravell) {
    include 'conex_wooapi.php';
    
    try {
        $result = $woocommerce->get('customers', array('meta' => [ 0 => ['value' => $id_caravell]]));
        return $result;
    } catch (Exception $e) {
        Logging($funcao='Buscar_cliente_meta_wooAPI', $status='erro');
    } 
}

// Inserir um produto via woocommerce e atualizar suas taxonomias se necessario
function Inserir_novo_produto_wooAPI($produto, $is_update=false) {
    // Guarda as taxonomias em variaveis auxiliares
    $em_destaque_em_promocao = null;
    $tipos_de_roupas = null;
    $colecao = null;

    if (array_key_exists('em_destaque_em_promocao', $produto)) {
        $em_destaque_em_promocao = $produto['em_destaque_em_promocao'];
        unset($produto['em_destaque_em_promocao']);
    }
    if (array_key_exists('tipos_de_roupas', $produto)) {
        $tipos_de_roupas = $produto['tipos_de_roupas'];
        unset($produto['tipos_de_roupas']);
    }
    if (array_key_exists('colecao', $produto)) {
        $colecao = $produto['colecao'];
        unset($produto['colecao']);
    }

    try {
        if ($is_update) {
            $prod_id = Buscar_id_pelo_sku_wooAPI(strval($produto['sku']));
            $response_api = Atualizar_wooAPI('products/'.$prod_id, $produto);
        } else {
            // Registra produto via woocommerce
            $response_api = Inserir_wooAPI('products', $produto);
        }
    } catch (Exception $e) {}

    // Prepa array no formato JSON
    $params = array();
    if ($em_destaque_em_promocao != null) {
        $params['em_destaque_em_promocao'] = $em_destaque_em_promocao;
    }
    if ($tipos_de_roupas != null) {
        $params['tipos_de_roupas'] = $tipos_de_roupas;
    }
    if ($colecao != null) {
        $params['colecao'] = $colecao;
    }

    //Se tem taxonomia atauliza wp APi
    if (count($params) > 0) {
        return Atualizar_taxonomia_de_um_produto_wpAPi($response_api->id, $params);
    }
    // Caso não haja taxonomia retorna a resposta do woocommerce
    return $response_api;
}

function Atualizar_taxonomia_de_um_produto_wpAPi($id_produto, $taxonomias) {
    try {
        include 'conex_wpapi.php';

        $response = $wordpress->put('wp-json/wp/v2/product/'.$id_produto, array('query' => $taxonomias));
        return json_decode($response->getBody());
    } catch (Exception $e) {
        Logging($funcao='Atualizar_taxonomia_de_um_produto_wpAPi', $status=$e);
    }
}

/**
 * Inserir todos os produtos do banco no woocommerce e worpress
 */
function Inserir_todos_produtos_disponiveis($insert_unico_sku=null, $is_update=false) {
    try {
        ini_set('max_execution_time', 100000); 
        // Colecao
        $colecao = Buscar_taxonomia_wpAPI($taxo_address='colecao/?per_page=99');
        $ids_colecao = [];
        foreach ($colecao as $colecao_row) {
            $ids_colecao[$colecao_row->name] = $colecao_row->id;
        }
    
        // Tipos de roupas
        $tip_roupas = Buscar_taxonomia_wpAPI($taxo_address='tipos_de_roupas/?per_page=99');
        $ids_tip_roupas = [];
        foreach ($tip_roupas as $tip_roupas_row) {
            $ids_tip_roupas[$tip_roupas_row->name] = $tip_roupas_row->id;
        }
    
        // Em destaque / Em promocao
        $destaq_promo = Buscar_taxonomia_wpAPI($taxo_address='em_destaque_em_promocao/?per_page=99');
        $ids_destaq_promo = [];
        foreach ($destaq_promo as $destaq_promo_row) {
            $ids_destaq_promo[$destaq_promo_row->slug] = $destaq_promo_row->id;
        }
    
        // Em destaque / Em promocao
        $categories = Buscar_wooAPI('products/categories');
        $ids_categories = [];
        foreach ($categories as $categories_row) {
            $ids_categories[$categories_row->slug] = $categories_row->id;
        }
    
        $sql = "SELECT i.NUMERO
                    , i.NOME
                    , i.VLVENDA3
                    , i.MASCUL
                    , i.FEMINI
                    , i.INFANT 
                    , i.PROMO
                    , i.DESTAQ
                    , i.LINHA
                    , lin.NOME
                    , i.COLECAO
                    , col.NOME
                    , i.STATUS
                FROM itens i
                    LEFT JOIN item_linha lin
                ON i.LINHA = lin.NUMERO
                    LEFT JOIN item_colecao col
                ON i.COLECAO = col.NUMERO";

        if ($insert_unico_sku != null) {
            $sql .= " WHERE i.NUMERO = ".$insert_unico_sku.";";
        } else {
            //ISSUE: A ser removido na versao final
            $sql .= " WHERE i.NUMERO >= 501 
                ORDER BY i.NUMERO ASC LIMIT 50;";
        }
        $result = Enviar_sql_bd($sql);
    
        # Linha=[9] e Colecao=[11]
        foreach ($result as $result_row) {
    
            /*                              PRODUC      | ESTOQU    | CONSUL        | INATIV
                [status]                => publish      | publish   | publish       | draft
                [manage_stock]          =>              | true      | true          |
                [stock_quantity]        =>              |           | 0             |
                [stock_status]          => instock      | instock   | onbackorder   | outofstock
                [backorders]            => no           | no        | notify        | no
                [backorders_allowed]    =>              |           | true          |
                [backordered]           =>              |           | true          |
            */
            // Padrao eh inativo
            $data = [
                'name'                      => $result_row['1']
                , 'regular_price'           => $result_row['VLVENDA3']
                , 'sku'                     => $result_row['NUMERO']
                , 'type'                    => 'variable'
                , 'status'                  => 'draft'
                , 'manage_stock'            => null
                , 'stock_quantity'          => null
                , 'stock_status'            => 'outofstock'
                , 'backorders'              => 'no'
                , 'backorders_allowed'      => null
                , 'backordered'             => null
                , 'categories'              => []
                , 'em_destaque_em_promocao' => []
                , 'tipos_de_roupas'         => []
                , 'colecao'                 => []
            ];

            //status
            // if ($result_row['STATUS'] === 'CONSUL') {
            //     $data['status']             = 'publish';
            //     $data['manage_stock']       = true;
            //     $data['stock_quantity']     = '0';
            //     $data['stock_status']       = 'onbackorder';
            //     $data['backorders']         = 'notify';
            //     $data['backorders_allowed'] = true;
            //     $data['backordered']        = true;
            // } 
            
            if ($result_row['STATUS'] === 'PRODUC' || $result_row['STATUS'] === 'ESTOQU') {
                $data['status'] = 'publish';
            }

            // categorias
            if (strcasecmp($result_row['MASCUL'], 'S') == 0) {
                $data['categories'][] = ['id' => $ids_categories['masculina']];
            }
            if (strcasecmp($result_row['FEMINI'], 'S') == 0) {
                $data['categories'][] = ['id' => $ids_categories['feminina']];
            }
            if (strcasecmp($result_row['INFANT'], 'S') == 0) {
                $data['categories'][] = ['id' => $ids_categories['infantil']];
            }

            // em destaque em promocao
            if (strcasecmp($result_row['PROMO'], 'S') == 0) {
                $data['em_destaque_em_promocao'] = [$ids_destaq_promo['em_promocao']];
            }
            if (strcasecmp($result_row['DESTAQ'], 'S') == 0) {
                $data['em_destaque_em_promocao'] = [$ids_destaq_promo['em_destaque']];
            }

            // tipos de roupas
            if($result_row['LINHA'] != null) {
                $data['tipos_de_roupas'] = [$ids_tip_roupas[$result_row[9]]];
            }

            // colecao
            if($result_row['COLECAO'] != null) {
                $data['colecao'] = [$ids_colecao[$result_row[11]]];
            }
    
            if ($is_update || $insert_unico_sku != null) {
                return Inserir_novo_produto_wooAPI($data, $is_update);;
            } else {
                Inserir_novo_produto_wooAPI($data, $is_update);
            }
        }
    } catch (Exception $e) {
        Logging($funcao='Inserir_todos_produtos_disponiveis', $status=$e);
    }
}


/**
 * CRUD: Atributos de um produto
 * https://florianbrinkmann.com/en/4526/creating-a-woocommerce-product-variation-rest-api/
 */

// Funcao Inserir e Atualizar
// 'options' => [ 'option-A', 'option-B', 'option-C' ]
function Inserir_atributos_em_um_produto_wooAPI($id_produto, $data) {
    $data_end = ['attributes' => $data];
    $address = 'products/'.$id_produto;
    return Inserir_wooAPI($address, $data_end);
}

function Atualizar_atributos_em_um_produto_wooAPI($id_produto, $atributos) {
    $data = ['attributes' => $atributos];
    $address = 'products/'.$id_produto;
    return Atualizar_wooAPI($address, $data);
}

/**
 * Inserir todos os atributos de produtos do banco no woocommerce e worpress
 */
function Inserir_todos_atributos_de_produtos_disponiveis($insert_unico_sku=null) {
    ini_set('max_execution_time', 1000000); 

    // Cria a categoria de tributo
    $data_batch = [
        'create' => [
            [
                'name' => 'Tamanho'
                , 'type' => 'color'
            ],
            [   
                'name' => 'Cor / Tecido'
                , 'type' => 'color'
            ]
        ]
    ];
    $batch_result = Batch_wooAPI('products/attributes', $data_batch);

    // Incia processo para adiconar um atributo em cada produto
    $sku_produtos = Selecionar_bd('itens');

    $data = ['update' => []];
    $index = 0;
    foreach ($sku_produtos as $sku_row) {
        $prod_id = Buscar_id_pelo_sku_wooAPI(strval($sku_row[0]));

        $sql = "SELECT lis.ITEM
                    , lis.NUMERO
                    , lis.TAMANHO
                    , lis.COR
                    , c.NOME
                    , lis.QTD
                FROM item_lis lis
                    LEFT JOIN cores c
                ON lis.COR = c.NUMERO
                WHERE lis.ITEM = ".$sku_row[0]."";

        $atributos = Enviar_sql_bd($sql);

        if (count($atributos) <= 0) {
            continue;
        }
        
        // utilizacao de sets para atributos sem repeticao
        $tamanho = $cor = [];
        // guarda todos os atributos
        $options_cor = $options_tam = [];
        foreach ($atributos as $att_row) {
            if ($att_row['TAMANHO'] != null) {
                $tamanho[$att_row['TAMANHO']] = $att_row['TAMANHO'];
            }
            
            if ($att_row['NOME'] != null) {
                $cor[$att_row['NOME']] = $att_row['NOME'];
            }
        }
        foreach ($tamanho as $row) {
            $options_tam[] = $row;
        }
        foreach ($cor as $row) { 
            $options_cor[] = $row;
        }
        

        // define qual produto vai receber os atributos
        $data['update'][$index]['id'] = $prod_id;
        // setta tamanho
        if ($options_tam != null && count($options_tam) > 0) {
            $data['update'][$index]['attributes'][] = [
                'id' => $batch_result->create[0]->id,
                'visible' => true,
                'variation' => true, 
                'options' => $options_tam
            ];
        }
        // setta cor
        if ($options_cor != null && count($options_cor) > 0) {
            $data['update'][$index]['attributes'][] = [
                'id' => $batch_result->create[1]->id,
                'visible' => true,
                'variation' => true, 
                'options' => $options_cor
            ];
        }

        if($index == 99) {        
            Batch_wooAPI('products', $data);
            $data = ['update' => []];
            $index = 0;
        } else {
            $index++;
        }
    }

    print_r(Batch_wooAPI('products', $data));
}

function Buscar_produto_atributos_wooAPI() {
    include 'conex_wooapi.php';

    try {
        $result = $woocommerce->get('products/attributes/2/terms');
        return $result;
    } catch (Exception $e) {
        Logging($funcao='Buscar_produto_atributos_wooAPI', $status='erro');
    }
}


/**
 * CRUD: Variações de um Produto
 */
/**
 * Inserir todos as variacoes de produtos do banco no woocommerce e worpress
 */
function Inserir_todas_variacoes_de_produtos_disponiveis() {
    ini_set('max_execution_time', 1000000); 
    // utilizado para pegar a sku de produtos
    $sku_produtos = Selecionar_bd('itens');
    // utilizado para pegar o id de atributos
    $id_atributos = Buscar_wooAPI('products/attributes');

    foreach ($sku_produtos as $sku_row) {
        $prod_id = Buscar_id_pelo_sku_wooAPI(strval($sku_row[0]));

        $sql = "SELECT lis.ITEM
                , lis.NUMERO
                , lis.TAMANHO
                , lis.COR
                , c.NOME
                , lis.QTD
            FROM item_lis lis
                INNER JOIN cores c
            ON lis.COR = c.NUMERO
            WHERE lis.ITEM = ".$sku_row[0].";";

        $variacoes = Enviar_sql_bd($sql);

        if (count($variacoes) <= 0) {
            continue;
        }

        $data_end = ['create' => []];
        $index = 0;
        foreach ($variacoes as $var_row) {
            try {
                $data_end['create'][$index] = Mapear_variacao_data_para_insercao_update_wooAPI($id_atributos, $sku_row, $var_row);
            } catch (Exception $e) {
                Logging($funcao='Inserir_todas_variacoes_de_produtos_disponiveis', $status='erro: ao mapear.');
                continue;
            }

            if($index == 99) {
                Batch_wooAPI('products/'.$prod_id.'/variations', $data_end);
                $data_end = ['create' => []];
                $index = 0;
            } else {
                $index++;
            }
        }
        Batch_wooAPI('products/'.$prod_id.'/variations', $data_end);
    }
}

function Inserir_atualizar_variacao_registrada_wooAPI($insert_unico_sku, $is_update=false) {

    $sku_splited = explode("-", $insert_unico_sku);
    $item_sku = $sku_splited[0];
    $numero_sku = $sku_splited[1];

    // utilizado para pegar o id de um produto
    $prod_id = Buscar_id_pelo_sku_wooAPI(strval($item_sku));
    // Utilizado para pegar o status de um produto
    $root = Selecionar_id_bd('itens', $item_sku);
    // utilizado para pegar o id de atributos
    $id_atributos = Buscar_wooAPI('products/attributes');

    $sql = "SELECT lis.ITEM
            , lis.NUMERO
            , lis.TAMANHO
            , lis.COR
            , c.NOME
            , lis.QTD
        FROM item_lis lis
            LEFT JOIN cores c
        ON lis.COR = c.NUMERO
        WHERE lis.ITEM = ".$item_sku." and lis.NUMERO = ".$numero_sku.";";
    $variacao = Enviar_sql_bd($sql);

    if($variacao[0]['TAMANHO'] == null || ($variacao[0]['COR'] == null || $variacao[0]['NOME'] == null)) {
        Logging($funcao='Inserir_atualizar_variacao_registrada_wooAPI', $status='erro: algum atributo nulo.');
        return;
    }

    try {     
        $root_att = Buscar_id_wooAPI('products', $prod_id)->attributes;
        // Caso nao haja nenhum atributo neste produto
        if (count($root_att) <= 0) {
            $root_att = [
                1 => [
                    'id'        => $id_atributos[1]->id,
                    'visible'   => true, 
                    'variation' => true,
                    'options'   => [$variacao[0]['TAMANHO']]
                ],
                0 => [
                    'id'        => $id_atributos[0]->id,
                    'visible'   => true, 
                    'variation' => true,
                    'options'   => [$variacao[0]['NOME']]
                ]
            ];
            Inserir_atributos_em_um_produto_wooAPI($prod_id, $root_att);
        }
        // Se houver atributos ele insere um novo (soh ira inserir o atributo se este for novo - woocommerce configuration) 
        else {
            if ($root_att[0]->name === 'Tamanho') {
                $root_att[0]->options[] = $variacao[0]['TAMANHO'];
                $root_att[1]->options[] = $variacao[0]['NOME'];
            } else {
                $root_att[0]->options[] = $variacao[0]['NOME'];
                $root_att[1]->options[] = $variacao[0]['TAMANHO'];
            }

            $array_att = [
                json_decode(json_encode($root_att[1]), True)
                , json_decode(json_encode($root_att[0]), True)
            ];

            // Atualiza atributos
            Atualizar_atributos_em_um_produto_wooAPI($prod_id, $root_att);
        }
    } catch (Exception $e) {
        Logging($funcao='Inserir_atualizar_variacao_registrada_wooAPI', $status='erro: erro ao tentar atualizar atributos.');
        return;
    }
    
    $data = Mapear_variacao_data_para_insercao_update_wooAPI($id_atributos, $root[0], $variacao[0]);

    if ($is_update) {
        $var_id = Buscar_id_pelo_sku_wooAPI(strval($insert_unico_sku));
        return Atualizar_wooAPI('products/'.$prod_id.'/variations/'.$var_id, $data);
    } else {
        return Inserir_wooAPI('products/'.$prod_id.'/variations', $data);
    }
}

function Mapear_variacao_data_para_insercao_update_wooAPI($id_atributos, $root, $var_row) {

    $data = [];
    // setta sku
    $data['sku'] = $var_row['ITEM']."-".$var_row['NUMERO'];

    // setta status
    switch ($root['STATUS']) {
        case 'PRODUC':
            $data['stock_status'] = 'instock';
            break;
        case 'ESTOQU':
            $data['stock_status'] = true;
            $data['stock_quantity'] = $var_row['QTD'];
            $data['stock_status'] = 'instock';
            break;
        case 'INATIV':
            $data['stock_status'] = 'outofstock';
            break;
    }

    // setta att tamanho
    if ($var_row['TAMANHO'] != null) {
        $data['attributes'][] = [
            'id' => $id_atributos[1]->id,
            "option" => $var_row['TAMANHO']
        ];
    } else {
        return;
    }
    // setta att cor
    if ($var_row['NOME'] != null) {
        $data['attributes'][] = [
            'id' => $id_atributos[0]->id,
            "option" => $var_row['NOME']
        ];
    } else {
        return;
    }

    return $data;
}

function Buscar_produto_variacoes_wooAPI($id) {
    include 'conex_wooapi.php';

    try {
        $variations = $woocommerce->get('products/'.$id.'/variations');
        return $variations;
    } catch (Exception $e) {
        Logging($funcao='Buscar_produto_variacoes_wooAPI', $status='erro');
    }
}

function Buscar_produto_variacao_id_wooAPI($id_produto, $id_variacao) {
    include 'conex_wooapi.php';

    try {
        $variation = $woocommerce->get('prducts/'.$id_produto.'/variations/'.$id_variacao);
        return $variation;
    } catch (Exception $e) {
        Logging($funcao='Buscar_produto_variacao_id_wooAPI', $status='erro');
    }
}

function Verificar_variacao_wooAPI($id_produto, $id_variacao) {
    include 'conex_wooapi.php';
    
    try {
        if ($id_produto == 49 && $id_variacao == 755) {
            return true;
        }
        return false;
    } catch (Exception $e) {
        Logging($funcao='Verificar_variacao_wooAPI', $status='erro');
    }
}

/**
 * CRUD: Taxonomias de um produto
 */
function Inserir_taxonomia_wpAPI($taxo_name, $table_caravell, $id_caravell) {
    try {
        include 'conex_wpapi.php';

        /**
          TROCAR PROXIMAS DUAS LINHAS PELO MEDOTO: Selecionar_bd()
         */
        $sql = "SELECT * FROM $table_caravell WHERE NUMERO = $id_caravell";
        $taxo_row = Enviar_sql_bd($sql);
        $params = [
            'name' => $taxo_row[0]['NOME']
        ];
        $response = json_decode($wordpress->post('wp-json/wp/v2/'.$taxo_name, ['query' => $params])->getBody());
        $result = $wordpress->put('/wp-json/wp/v2/'.$taxo_name.'/id-caravell/put/'.$response->id.'/'.$id_caravell);
        return json_decode($result->getBody());
    } catch (Exception $e) {
        Logging($funcao='Inserir_taxonomia_wpAPI ==> @taxo: '.$taxo_name, $status=$e);
    }
}

function Atualizar_taxonomia_wpAPI($taxo_name, $table_caravell, $id_caravell, $target) {
    try {
        include 'conex_wpapi.php';

        $sql = "SELECT * FROM $table_caravell WHERE NUMERO = $id_caravell";
        $taxo_row = Enviar_sql_bd($sql);
        $params = [
            'name' => $taxo_row[0]['NOME']
        ];
        $response = $wordpress->put('wp-json/wp/v2/'.$taxo_name.'/'.$target, ['query' => $params]);
        return json_decode($response->getBody());
    } catch (Exception $e) {
        Logging($funcao='Atualizar_taxonomia_wpAPI ==> @taxo: '.$taxo_name, $status=$e);
    }
}

function Buscar_taxonomia_wpAPI($taxo_name) {
    try {
        include 'conex_wpapi.php';

        $params = ['per_page' => 99];
        $response = $wordpress->get('wp-json/wp/v2/'.$taxo_name, ['query' => $params]);
        return json_decode($response->getBody());
    } catch (Exception $e) {
        Logging($funcao='Buscar_taxonomia_wpAPI - '.$taxo_name, $status='erro');
    }
}

function Buscar_taxonomia_id_wpAPI($taxo_name, $id) {
    try {
        include 'conex_wpapi.php';

        $response = $wordpress->get('wp-json/wp/v2/'.$taxo_name.'/'.$id);
        return json_decode($response->getBody());
    } catch (Exception $e) {
        Logging($funcao='Buscar_taxonomia_id_wpAPI - '.$taxo_name, $status='erro');
    }
}

function Buscar_taxonomia_pelo_idcaravell_wpAPI($table_name, $target) {
    try {
        $linhas = Buscar_taxonomia_wpAPI($table_name);
        foreach ($linhas as $row) {
            if($row->id_caravell == $target) {
                return $row;
            }
        }
    } catch (Exception $e) {
        Logging($funcao='Buscar_taxonomia_pelo_idcaravell_wpAPI - '.$table_name, $status='erro');
    }
}

function Delete_taxonomia_wpAPI($taxo_name, $target) {
    try {
        include 'conex_wpapi.php';

        $params = ['force' => true];
        $response = $wordpress->delete('wp-json/wp/v2/'.$taxo_name.'/'.$target, ['query' => $params]);
        return json_decode($response->getBody());
    } catch (Exception $e) {
        Logging($funcao='Atualizar_taxonomia_wpAPI ==> @taxo: '.$taxo_name.' ==> @nome: '.$nome, $status=$e);
    }
}

// @taxo_name: item_colecao / item_linha (tipos_de_roupas)
function Inserir_todas_toxonomias_disponiveis($table_caravell) {
    $result = [];
    if ($table_caravell === 'item_linha') {
        $taxo_name = 'tipos_de_roupas';
    } else if ($table_caravell === 'item_colecao') {
        $taxo_name = 'colecao';
    }
    $sql = "SELECT * FROM $table_caravell;";
    $itens = Enviar_sql_bd($sql);
    foreach ($itens as $row) {
        $result[] = Inserir_taxonomia_wpAPI($taxo_name, $table_caravell, $row['NUMERO']);
    }
    return $result;
}

/**
 * CRUD: Clientes
 * OBS: 
 *      precisa-se adicionar email na tabela clientes.
 *      precisa-se implementar email elastico.
 */
function Inserir_cliente_wooAPI($id_cliente, $is_update=null) {
    $sql = "SELECT NUMERO
            , NOME
            , TIPO
        FROM clientes WHERE NUMERO = ".$id_cliente." AND TIPO = 7;";
    $cliente = Enviar_sql_bd($sql);

    if (count($cliente) <= 0) {
        Logging($funcao='Inserir_cliente', $status='Erro: Cliente inserido não é do tipo 7.');
        return false;
    }

    $full_name = explode(' ', $cliente[0]['NOME']);
    $email = null; // Email needed.
    $first_name = $last_name = $username = $password = '';
    $ind = 0;
    foreach ($full_name as $row) {
       if ($ind == 0) {
            $first_name = $row;
            $username = $row;
            $ind++;
       } else {
            if ($ind == (count($full_name)-1)) {
                $last_name .= $row;
                $username .= '.'.$row;
            } else {
                $last_name .= $row.' ';
            }
            $ind++;
       }
    }
    $password = $username.'.'.$cliente[0]['NUMERO'];

    $data;
    if ($is_update != null) {
        $data = [
            'email' => $first_name.'@example.com' //$email
            , 'first_name'  => $first_name
            , 'last_name'   => $last_name
        ];
        $client_wooAPi = Buscar_cliente_meta_wooAPI($id_cliente);
        return Atualizar_wooAPI('customers/', $data);
    } else {
        $data = [
            'email' => $first_name.'@example.com' //$email
            , 'first_name'  => $first_name
            , 'last_name'   => $last_name
            , 'username'    => $username
            , 'password'    => $password
            , 'meta_data'   => [ 
                [
                    'key'       => 'id_caravell'
                    , 'value'   => $cliente[0]['NUMERO']
                ]
            ]
        ];
        return Inserir_wooAPI('customers', $data);
    }

    
}

function Atualizar_cliente_wooAPI($id_caravell) {
    
}

/*****************************
 * FUNCOES DO BANCO DE DADOS *
 *****************************/

function Enviar_sql_bd($sql) {
    include 'conex_bd.php';
    
    try {
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    } catch (Exception $e) {
        Logging($funcao='Enviar_sql_bd, sql: '.$sql, $status=$e);
    }  
}

function Selecionar_bd($tabela) {
    $sql = "SELECT * FROM $tabela";
    $sql .= " WHERE NUMERO >= 501 
        ORDER BY NUMERO ASC LIMIT 50;";
    return Enviar_sql_bd($sql);
}

function Selecionar_id_bd($tabela, $id) {
    $sql = "SELECT * FROM $tabela WHERE NUMERO='$id'";
    return Enviar_sql_bd($sql);     
}

function Atualizar_bd($tabela, $id, $coluna, $dado) {
    include 'conex_bd.php';
    
    try {
        $sql = "UPDATE $tabela SET $coluna='$dado' WHERE id=$id";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
    } catch (Exception $e) {
        Logging($funcao='Enviar_sql_bd, sql: '.$sql, $status=$e);
    } 
}

/*****************************
 *          SCRIPTS          *
 *****************************/
/**
 * Usa-se somente para popular o site pela primeira vez
 */
function Script_inicial() {
    // 0 - Delecao de todos os produtos da loja
    try {
        while(true) {
            $products = Buscar_wooAPI('products');
            if(count($products) == 0) {
                break;
            }
            $del = ['delete' => []];
            foreach ($products as $row) {
                $del['delete'][] = $row->id;
            }
            Batch_wooAPI('products', $del);
        }
        while(true) {
            $tip_roupas = Buscar_taxonomia_wpAPI('tipos_de_roupas');
            if(count($tip_roupas) == 0) {
                break;
            }
            foreach ($tip_roupas as $row) {
                Delete_taxonomia_wpAPI('tipos_de_roupas', $row->id);
            }
        }
        while(true) {
            $colecao = Buscar_taxonomia_wpAPI('colecao');
            if(count($colecao) == 0) {
                break;
            }
            foreach ($colecao as $row) {
                Delete_taxonomia_wpAPI('colecao', $row->id);
            }
        }
    } catch (Exception $e) {
        Logging($funcao='Script_inicial()', $status='0 - Delecao produtos: ERRO');
    } finally {
        Logging($funcao='Script_inicial()', $status='0 - Delecao produtos: realizado com sucesso!');
    }
    // 1 - inserir taxonomias (item_colecao / item_linha):
    try {
        Inserir_todas_toxonomias_disponiveis($taxo_name='item_colecao');
        Inserir_todas_toxonomias_disponiveis($taxo_name='item_linha');
    } catch (Exception $e) {
        Logging($funcao='Script_inicial()', $status='1 - inserir taxonomias: ERRO');
    } finally {
        Logging($funcao='Script_inicial()', $status='1 - inserir taxonomias: realizado com sucesso!');
    }
    // 2 - inserir produtos:
    try {
        Inserir_todos_produtos_disponiveis();
    } catch (Exception $e) {
        Logging($funcao='Script_inicial()', $status='2 - inserir produtos: ERRO');
    } finally {
        Logging($funcao='Script_inicial()', $status='2 - inserir produtos: realizado com sucesso!');
    }
    // 3 - inserir atributos:
    try {
        Inserir_todos_atributos_de_produtos_disponiveis();
    } catch (Exception $e) {
        Logging($funcao='Script_inicial()', $status='3 - inserir atributos: ERRO');
    } finally {
        Logging($funcao='Script_inicial()', $status='3 - inserir atributos: realizado com sucesso!');
    }
    // 4 - inserir variacoes:
    try {
        Inserir_todas_variacoes_de_produtos_disponiveis();
    } catch (Exception $e) {
        Logging($funcao='Script_inicial()', $status='4 - inserir variacoes: ERRO');
    } finally {
        Logging($funcao='Script_inicial()', $status='4 - inserir variacoes: realizado com sucesso!');
    }
}

/**
 * Script de modificações que ocorrem no banco e atualiza na API do wp e woocommerce
 */
function Update_site() {
    $sql = "SELECT * FROM modificacoes WHERE status = 0;";
    $modificacoes = Enviar_sql_bd($sql);

    foreach ($modificacoes as $mod_row) {
        $result = null;
        switch ($mod_row['tabela']) {
            // Caso seja uma modificacao em produtos
            case 'itens':
                if ($mod_row['acao'] === 'INSERT') {
                    $result = Inserir_todos_produtos_disponiveis($mod_row['id_tabela']);
                } else if ($mod_row['acao'] === 'UPDATE') {
                    $result = Inserir_todos_produtos_disponiveis($mod_row['id_tabela'], $is_update=true);
                } else if ($mod_row['acao'] === 'DELETE') {
                    $prod_id = Buscar_produto_sku_wooAPI($mod_row['id_tabela']);
                    $result = Deletar_wooAPI('products/'.$prod_id[0]->id);
                }
                break;

            // Caso seja uma modificacao em variacoes
            case 'item_lis':
                if ($mod_row['acao'] === 'INSERT') {
                    $result = Inserir_atualizar_variacao_registrada_wooAPI($mod_row['id_tabela']);
                } else if ($mod_row['acao'] === 'UPDATE') {
                    $result = Inserir_atualizar_variacao_registrada_wooAPI($mod_row['id_tabela'], $is_update=true);
                } else if ($mod_row['acao'] === 'DELETE') {
                    $prod_id = Buscar_produto_sku_wooAPI($mod_row['id_tabela']);
                    $result = Deletar_wooAPI('products/'.$prod_id[0]->parent_id.'/variations/'.$prod_id[0]->id);
                }
                break;
            
            // Caso seja uma modificacao em tipos de roupas
            case 'item_linha':
                if ($mod_row['acao'] === 'INSERT') {
                    $result = Inserir_taxonomia_wpAPI('tipos_de_roupas', 'item_linha', $mod_row['id_tabela']);
                } else {
                    $target = Buscar_taxonomia_pelo_idcaravell_wpAPI('tipos_de_roupas', $mod_row['id_tabela']);

                    if ($mod_row['acao'] === 'UPDATE') {
                        $result = Atualizar_taxonomia_wpAPI('tipos_de_roupas', 'item_linha', $mod_row['id_tabela'], $target->id);
                    } else if ($mod_row['acao'] === 'DELETE') {
                        $result = Delete_taxonomia_wpAPI('tipos_de_roupas', $target->id);
                    }
                }
                break;

            // Caso seja uma modificacao em colecao
            case 'item_colecao':
                if ($mod_row['acao'] === 'INSERT') {
                    $result = Inserir_taxonomia_wpAPI('colecao', 'item_colecao', $mod_row['id_tabela']);
                } else {
                    $target = Buscar_taxonomia_pelo_idcaravell_wpAPI('colecao', $mod_row['id_tabela']);

                    if ($mod_row['acao'] === 'UPDATE') {
                        $result = Atualizar_taxonomia_wpAPI('colecao', 'item_colecao', $mod_row['id_tabela'], $target->id);
                    } else if ($mod_row['acao'] === 'DELETE') {
                        $result = Delete_taxonomia_wpAPI('colecao', $target->id);
                    }
                }
                break;
        }
        if ($result != null) {
            Atualizar_bd($tabela='modificacoes', $id=$mod_row['id'], $coluna='status', $dado=1);
        }
    } 
}


// /* Retornando apenas alguns atributos da tabela */
// function LerLogEtiqueta(){
//     include 'conex_wooapi.php';
//     $dados = array();
//     $a=$pdo_rds->query("SELECT * FROM etq_reimpressao");
//     $a->execute();
//     $result = $a->fetchAll();
//     foreach ($result as $row) {
//         $dados['id'][]=$row['id'];
//         $dados['order_id'][]=$row['order_id'];
//         $dados['motivo'][]=$row['motivo'];
//         $dados['via'][]=$row['via'];
//         $dados['data'][]=$row['data'];
//         $dados['usuario'][]=$row['usuario'];
//     }
//     return $dados;
// }

/* Cria log txt */
function Logging($funcao, $status, $obs=null) {
    header('Content-Type: text/html; charset=utf-8');
    $data_log = date("d_m_Y"); 
    $arquivo = "functions/logs/Log_$data_log.txt";
    $texto = "Funcao= ".$funcao." , Status= ".$status." , Obs= ".$obs.";\r\n"; 
    $manipular = fopen($arquivo, "a+"); 
    fwrite($manipular, $texto); 
    fclose($manipular); 
}

function Gerar_senha($qtd_caracter) {
	$aleatorio = rand($qtd_caracter,$qtd_caracter); // 12 a 12 CARACTERES
	$valor = substr(str_shuffle("ABCDEFGHIJKLMNPQRSTUVYXWZ123456789"), 0, $aleatorio);
	return $valor;	
}

//$subject= titulo
//$from= email que esta enviando, $to= email pra quem vai receber, $template=id tempalte do elastic email
//key= api key, $transctional=true or false, $sender=email de quem envia (mesmo que from), $msgtxt= email txt, $msghtml = email html
//campos obrigatorios = $from,$to,$key,$msghtml,$msgtxt,$isTransactional,$sender,$subject
function Enviar_email($from, $to, $key, $msghtml, $msgtxt, $isTransactional, $sender, $template, $subject){
	$result=array();
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://api.elasticemail.com/v2/email/send");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, true);

	$data = array(
	    'apikey' => $key,
	    'bodyText' => $msgtxt,
	    'bodyHtml' => $msghtml,
	    'from' => $from,
	    'isTransactional' => $isTransactional,
	    'sender' => $sender,
	    'to' => $to,
	    'template' => $template,
	    'subject' => $subject
	);

	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	$output = curl_exec($ch);
	$info = curl_getinfo($ch);
	curl_close($ch);
	
	if ($output) {
		$obj = json_decode($output);
		$result['status']=true;
		$result['transactionid']=$obj->data->transactionid;
		$result['messageid']=$obj->data->messageid;
		return $result;
	} else {
        echo '<pre>'; print_r(curl_error($ch)); 
    }
    $result['status']=false;
    $result['transactionid']=null;
    $result['messageid']=null;
    return $result;
}


//EnviarEmail($from='naoresponda@seox.com.br',$to='diogoparadela@gmail.com',$key='823d6a87-e548-4d8a-9d6e-57d39be13fbe',$msghtml='<p style="color: red;">dasdasd</p>',$msgtxt='ola',$isTransactional='true',$sender='naoresponda@seox.com.br',$template=null,$subject='venha comprar xbox'));




?>