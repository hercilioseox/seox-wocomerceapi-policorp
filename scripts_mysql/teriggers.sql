/* --------------------------------------   CREATE/ALTER TABLE   -------------------------------------- */
/**
 * DROPs tables
 */
 DROP TABLE IF EXISTS modificacoes;
 DROP TABLE IF EXISTS pedido_bkp;
 DROP TABLE IF EXISTS pedido_lis_bkp;

/**
 * CREATE coluna ip_woocommerce
 */
ALTER TABLE pedido ADD COLUMN id_woocommerce VARCHAR(255);


/**
 * CREATE/ALTER table modificacoes
 */
CREATE TABLE modificacoes (
  id int(11) NOT NULL,
  id_tabela varchar(255) NOT NULL,
  tabela varchar(255) NOT NULL,
  acao varchar(255) NOT NULL,
  data datetime DEFAULT NULL,
  status int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `modificacoes` ADD PRIMARY KEY (`id`);
ALTER TABLE `modificacoes` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


/**
 * CREATE/ALTER table pedido_bkp
 */
CREATE TABLE pedido_bkp (
  id int(11) NOT NULL,
  NUMERO int(11) NOT NULL DEFAULT 0,
  DTEMISSAO date DEFAULT NULL,
  DTENTREGA date DEFAULT NULL,
  DTATENDIDO date DEFAULT NULL,
  ATENDIDO tinyint(1) DEFAULT NULL,
  ORDEM int(11) DEFAULT NULL,
  CLIENTE int(11) DEFAULT NULL,
  ENDERECO smallint(6) DEFAULT NULL,
  TIPOPGTO smallint(6) DEFAULT NULL,
  VENDEDOR smallint(6) DEFAULT NULL,
  VLTOTAL decimal(14,4) DEFAULT NULL,
  NF int(11) DEFAULT NULL,
  OBS text,
  BAIXADO tinyint(1) DEFAULT NULL,
  DESCONTO varchar(10) DEFAULT NULL,
  CONSULTAR char(1) DEFAULT NULL,
  COMISSAO decimal(14,4) DEFAULT NULL,
  DESC1 decimal(14,4) DEFAULT NULL,
  DESC2 decimal(14,4) DEFAULT NULL,
  DESC3 decimal(14,4) DEFAULT NULL,
  DESC4 decimal(14,4) DEFAULT NULL,
  DESC5 decimal(14,4) DEFAULT NULL,
  TABELA smallint(6) DEFAULT NULL,
  VLFRETE decimal(14,4) DEFAULT NULL,
  CUPOM int(11) NOT NULL DEFAULT 0,
  TRANSPORTADORA smallint(6) NOT NULL DEFAULT 0,
  TIPO_NOTA int(11) NOT NULL DEFAULT 0,
  id_woocommerce varchar(20) DEFAULT NULL,
  acao varchar(50) DEFAULT NULL,
  data varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

ALTER TABLE pedido_bkp ADD PRIMARY KEY (id);
ALTER TABLE pedido_bkp MODIFY id int(11) NOT NULL AUTO_INCREMENT;


/**
 * CREATE/ALTER table pedido_lis_bkp
 */
 CREATE TABLE pedido_lis_bkp (
  id int(11) NOT NULL,
  PEDIDO int(11) NOT NULL DEFAULT '0',
  NUMERO smallint(6) NOT NULL DEFAULT '0',
  ITEM int(11) DEFAULT NULL,
  COR smallint(6) DEFAULT NULL,
  TAMANHO char(3) DEFAULT NULL,
  QTD_INICIAL decimal(14,4) DEFAULT NULL,
  QTD_FINAL decimal(14,4) DEFAULT NULL,
  VLUNITARIO decimal(14,4) DEFAULT NULL,
  ICMS decimal(14,4) DEFAULT NULL,
  ORDEM int(11) DEFAULT NULL,
  acao varchar(30) DEFAULT NULL,
  data varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

ALTER TABLE pedido_lis_bkp ADD PRIMARY KEY (id);
ALTER TABLE pedido_lis_bkp MODIFY id int(11) NOT NULL AUTO_INCREMENT;



/* -------------------------------------------   TRIGGERS   ------------------------------------------- */
/**
 * DROP TRIGGERS
 */
DROP TRIGGER IF EXISTS tag_itens_insert;
DROP TRIGGER IF EXISTS tag_itens_update;
DROP TRIGGER IF EXISTS tag_itens_delete;

DROP TRIGGER IF EXISTS tag_item_lis_insert;
DROP TRIGGER IF EXISTS tag_item_lis_update;
DROP TRIGGER IF EXISTS tag_item_lis_delete;

DROP TRIGGER IF EXISTS tag_cores_update;

DROP TRIGGER IF EXISTS tag_item_linha_insert;
DROP TRIGGER IF EXISTS tag_item_linha_update;
DROP TRIGGER IF EXISTS tag_item_linha_delete;

DROP TRIGGER IF EXISTS tag_item_colecao_insert;
DROP TRIGGER IF EXISTS tag_item_colecao_update;
DROP TRIGGER IF EXISTS tag_item_colecao_delete;

DROP TRIGGER IF EXISTS tag_clientes_insert;
DROP TRIGGER IF EXISTS tag_clientes_update;
DROP TRIGGER IF EXISTS tag_clientes_delete;

DROP TRIGGER IF EXISTS tag_pedido_insert;
DROP TRIGGER IF EXISTS tag_pedido_update;
DROP TRIGGER IF EXISTS tag_pedido_delete;

DROP TRIGGER IF EXISTS tag_pedido_lis_insert;
DROP TRIGGER IF EXISTS tag_pedido_lis_update;
DROP TRIGGER IF EXISTS tag_pedido_lis_delete;


/**
 * TRIGGERS itens
 * Logica: monitorar somente se o cliente que foi criado/atualizado/deletado é um revendedor
 */
/*INSERT itens*/
DELIMITER $$

CREATE TRIGGER tag_itens_insert
AFTER INSERT ON itens
FOR EACH ROW BEGIN
  IF (NEW.STATUS IS NOT NULL) THEN
      INSERT INTO modificacoes VALUES (null, NEW.NUMERO, 'itens', 'INSERT', now(), 0);
    END IF;
END;$$
DELIMITER ;

/*UPDATE itens*/
DELIMITER $$

CREATE TRIGGER tag_itens_update
AFTER UPDATE ON itens
FOR EACH ROW BEGIN
  IF (NEW.STATUS IS NOT NULL) THEN
      INSERT INTO modificacoes VALUES (null, NEW.NUMERO, 'itens', 'UPDATE', now(), 0);
    END IF;
END;$$
DELIMITER ;

/*DELETE itens*/
DELIMITER $$

CREATE TRIGGER tag_itens_delete
AFTER DELETE ON itens
FOR EACH ROW BEGIN
  IF (OLD.STATUS IS NOT NULL) THEN
      INSERT INTO modificacoes VALUES (null, OLD.NUMERO, 'itens', 'DELETE', now(), 0);
    END IF;
END;$$
DELIMITER ;


/**
 * TRIGGERS item_lis
 * Logica: monitorar somente se o status do item raiz nao for nulo 
 */
/*INSERT item_lis*/
DELIMITER $$

CREATE TRIGGER tag_item_lis_insert
AFTER INSERT ON item_lis
FOR EACH ROW BEGIN
  INSERT INTO modificacoes
    SELECT null, CONCAT(NEW.ITEM, '.', NEW.NUMERO), 'item_lis', 'INSERT',  NOW(), 0
        FROM DUAL
        WHERE EXISTS (
            SELECT *
            FROM item_lis il
                INNER JOIN itens i
            ON il.ITEM = i.NUMERO
            WHERE NEW.ITEM = il.ITEM AND NEW.NUMERO = il.NUMERO AND i.STATUS IS NOT NULL AND i.STATUS != 'INATIV'
        );
END;$$
DELIMITER ;

/*UPDATE item_lis*/
DELIMITER $$

CREATE TRIGGER tag_item_lis_update
AFTER UPDATE ON item_lis
FOR EACH ROW BEGIN
  INSERT INTO modificacoes
    SELECT null, CONCAT(NEW.ITEM, '.', NEW.NUMERO), 'item_lis', 'UPDATE',  NOW(), 0
        FROM DUAL
        WHERE EXISTS (
            SELECT *
            FROM item_lis il
                INNER JOIN itens i
            ON il.ITEM = i.NUMERO
            WHERE NEW.ITEM = il.ITEM AND NEW.NUMERO = il.NUMERO AND i.STATUS IS NOT NULL AND i.STATUS != 'INATIV'
        );
END;$$
DELIMITER ;

/*DELETE item_lis*/
DELIMITER $$

CREATE TRIGGER tag_item_lis_delete
BEFORE DELETE ON item_lis
FOR EACH ROW BEGIN
  INSERT INTO modificacoes
    SELECT null, CONCAT(OLD.ITEM, '.', OLD.NUMERO), 'item_lis', 'DELETE',  NOW(), 0
        FROM DUAL
        WHERE EXISTS (
            SELECT *
            FROM item_lis il
                INNER JOIN itens i
            ON il.ITEM = i.NUMERO
            WHERE OLD.ITEM = il.ITEM AND OLD.NUMERO = il.NUMERO AND i.STATUS IS NOT NULL AND i.STATUS != 'INATIV'
        );
END;$$
DELIMITER ;


/**
 * TRIGGERS cores
 * Pq somente update: pois eu soh insiro um atributos se há variacoes utilizando-o
 * Logica: monitorar todas as modificacoes na tabela
 */
/*UPDATE cores*/
DELIMITER $$

CREATE TRIGGER tag_cores_update
BEFORE UPDATE ON cores
FOR EACH ROW BEGIN
  INSERT INTO modificacoes
      SELECT null, CONCAT(NEW.NUMERO, '.', OLD.NOME), 'cores', 'UPDATE', now(), 0
        FROM DUAL
        WHERE EXISTS (
            SELECT 
                c.NOME
            FROM cores c
                INNER JOIN item_lis il
            ON il.cor = c.NUMERO
                INNER JOIN itens i
            ON i.NUMERO = il.ITEM
            WHERE i.STATUS IS NOT NULL AND i.STATUS != 'INATIV' AND NEW.NUMERO = c.NUMERO
        );
END;$$
DELIMITER ;


/**
 * TRIGGERS item_linha
 * Logica: monitorar todas as modificacoes na tabela
 */
/*INSERT item_linha*/
DELIMITER $$

CREATE TRIGGER tag_item_linha_insert
AFTER INSERT ON item_linha
FOR EACH ROW BEGIN
  INSERT INTO modificacoes VALUES (null, NEW.NUMERO, 'item_linha', 'INSERT', now(), 0); 
END;$$
DELIMITER ;

/*UPDATE item_linha*/
DELIMITER $$

CREATE TRIGGER tag_item_linha_update
AFTER UPDATE ON item_linha
FOR EACH ROW BEGIN
  INSERT INTO modificacoes VALUES (null, NEW.NUMERO, 'item_linha', 'UPDATE', now(), 0); 
END;$$
DELIMITER ;

/*DELETE item_linha*/
DELIMITER $$

CREATE TRIGGER tag_item_linha_delete
AFTER DELETE ON item_linha
FOR EACH ROW BEGIN
  INSERT INTO modificacoes VALUES (null, OLD.NUMERO, 'item_linha', 'DELETE', now(), 0); 
END;$$
DELIMITER ;


/**
 * TRIGGERS item_colecao
 * Logica: monitorar todas as modificacoes na tabela
 */
/*INSERT item_colecao*/
DELIMITER $$

CREATE TRIGGER tag_item_colecao_insert
AFTER INSERT ON item_colecao
FOR EACH ROW BEGIN
  INSERT INTO modificacoes VALUES (null, NEW.NUMERO, 'item_colecao', 'INSERT', now(), 0);
END;$$
DELIMITER ;

/*UPDATE item_colecao*/
DELIMITER $$

CREATE TRIGGER tag_item_colecao_update
AFTER UPDATE ON item_colecao
FOR EACH ROW BEGIN
  INSERT INTO modificacoes VALUES (null, NEW.NUMERO, 'item_colecao', 'UPDATE', now(), 0); 
END;$$
DELIMITER ;

/*DELETE item_colecao*/
DELIMITER $$

CREATE TRIGGER tag_item_colecao_delete
AFTER DELETE ON item_colecao
FOR EACH ROW BEGIN
  INSERT INTO modificacoes VALUES (null, OLD.NUMERO, 'item_colecao', 'DELETE', now(), 0); 
END;$$
DELIMITER ;


/**
 * TRIGGERS clientes
 * Logica: monitorar somente se o cliente que foi criado/atualizado/deletado é um revendedor
 */
/*INSERT clientes*/
DELIMITER $$

CREATE TRIGGER tag_clientes_insert
AFTER INSERT ON clientes
FOR EACH ROW BEGIN
  IF (NEW.TIPO = 7) THEN
      INSERT INTO modificacoes VALUES (null, NEW.NUMERO, 'clientes', 'INSERT', now(), 0);
    END IF;
END;$$
DELIMITER ;

/*UPDATE clientes*/
DELIMITER $$

CREATE TRIGGER tag_clientes_update
AFTER UPDATE ON clientes
FOR EACH ROW BEGIN
  IF (NEW.TIPO = 7) THEN
      INSERT INTO modificacoes VALUES (null, NEW.NUMERO, 'clientes', 'UPDATE', now(), 0);
    END IF;
END;$$
DELIMITER ;

/*DELETE clientes*/
DELIMITER $$

CREATE TRIGGER tag_clientes_delete
AFTER DELETE ON clientes
FOR EACH ROW BEGIN
  IF (OLD.TIPO = 7) THEN
      INSERT INTO modificacoes VALUES (null, OLD.NUMERO, 'clientes', 'DELETE', now(), 0); 
    END IF;
END;$$
DELIMITER ;


/**
 * TRIGGERS enderecos
 * Logica: monitorar somente se o endereco de cliente revendedor foi atualizado 
 */
/*UPDATE enderecos*/
DELIMITER $$

CREATE TRIGGER tag_enderecos_update
AFTER UPDATE ON enderecos
FOR EACH ROW BEGIN
  IF (NEW.EMAIL != OLD.EMAIL) THEN
    INSERT INTO modificacoes
      SELECT null, NEW.CLIENTE, 'clientes', 'UPDATE', now(), 0
          FROM DUAL
          WHERE EXISTS (
              SELECT *
              FROM enderecos e
                  INNER JOIN clientes c
              ON c.NUMERO = e.CLIENTE
              WHERE NEW.NUMERO = e.NUMERO AND c.TIPO = 7
          );
  END IF;
END;$$
DELIMITER ;


/**
 * TRIGGERS pedido
 * Logica: monitorar somente se o pedido foi criado/atualizado/deletado por um cliente revendedor
 */
/*INSERT pedido*/
DELIMITER $$

CREATE TRIGGER tag_pedido_insert
AFTER INSERT ON pedido
FOR EACH ROW BEGIN
  INSERT INTO modificacoes
    SELECT null, NEW.NUMERO, 'pedido', 'INSERT', now(), 0
        FROM DUAL
        WHERE EXISTS (
            SELECT *
            FROM pedido p
                INNER JOIN clientes c
            ON c.NUMERO = p.CLIENTE
            WHERE NEW.NUMERO = p.NUMERO AND c.TIPO = 7
        );
    INSERT INTO pedido_bkp VALUES (null, NEW.NUMERO, NEW.DTEMISSAO, NEW.DTENTREGA, NEW.DTATENDIDO, NEW.ATENDIDO, NEW.ORDEM, NEW.CLIENTE, NEW.ENDERECO, NEW.TIPOPGTO, NEW.VENDEDOR, NEW.VLTOTAL, NEW.NF, NEW.OBS, NEW.BAIXADO, NEW.DESCONTO, NEW.CONSULTAR, NEW.COMISSAO, NEW.DESC1, NEW.DESC2, NEW.DESC3, NEW.DESC4, NEW.DESC5, NEW.TABELA, NEW.VLFRETE, NEW.CUPOM, NEW.TRANSPORTADORA, NEW.TIPO_NOTA, NEW.id_woocommerce, 'INSERT', now()); 
END;$$
DELIMITER ;

/*UPDATE pedido*/
DELIMITER $$

CREATE TRIGGER tag_pedido_update
AFTER UPDATE ON pedido
FOR EACH ROW BEGIN
  INSERT INTO modificacoes
    SELECT null, NEW.NUMERO, 'pedido', 'UPDATE', now(), 0
        FROM DUAL
        WHERE EXISTS (
            SELECT *
            FROM pedido p
                INNER JOIN clientes c
            ON c.NUMERO = p.CLIENTE
            WHERE NEW.NUMERO = p.NUMERO AND c.TIPO = 7
        ) AND NOT EXISTS (
            SELECT COUNT(*) AS cnt,id,status from modificacoes WHERE id_tabela=NEW.NUMERO AND status=0 group by status having cnt > 0
        );
    INSERT INTO pedido_bkp VALUES (null, NEW.NUMERO, NEW.DTEMISSAO, NEW.DTENTREGA, NEW.DTATENDIDO, NEW.ATENDIDO, NEW.ORDEM, NEW.CLIENTE, NEW.ENDERECO, NEW.TIPOPGTO, NEW.VENDEDOR, NEW.VLTOTAL, NEW.NF, NEW.OBS, NEW.BAIXADO, NEW.DESCONTO, NEW.CONSULTAR, NEW.COMISSAO, NEW.DESC1, NEW.DESC2, NEW.DESC3, NEW.DESC4, NEW.DESC5, NEW.TABELA, NEW.VLFRETE, NEW.CUPOM, NEW.TRANSPORTADORA, NEW.TIPO_NOTA, NEW.id_woocommerce, 'UPDATE', now());
END;$$
DELIMITER ;

/*DELETE pedido*/
DELIMITER $$

CREATE TRIGGER tag_pedido_delete
BEFORE DELETE ON pedido
FOR EACH ROW BEGIN
  INSERT INTO modificacoes
    SELECT null, CONCAT(OLD.NUMERO, '.', OLD.id_woocommerce), 'pedido', 'DELETE', now(), 0
        FROM DUAL
        WHERE EXISTS (
            SELECT *
            FROM pedido p
                INNER JOIN clientes c
            ON c.NUMERO = p.CLIENTE
            WHERE OLD.NUMERO = p.NUMERO AND c.TIPO = 7
        );
  INSERT INTO pedido_bkp VALUES (null, OLD.NUMERO, OLD.DTEMISSAO, OLD.DTENTREGA, OLD.DTATENDIDO, OLD.ATENDIDO, OLD.ORDEM, OLD.CLIENTE, OLD.ENDERECO, OLD.TIPOPGTO, OLD.VENDEDOR, OLD.VLTOTAL, OLD.NF, OLD.OBS, OLD.BAIXADO, OLD.DESCONTO, OLD.CONSULTAR, OLD.COMISSAO, OLD.DESC1, OLD.DESC2, OLD.DESC3, OLD.DESC4, OLD.DESC5, OLD.TABELA, OLD.VLFRETE, OLD.CUPOM, OLD.TRANSPORTADORA, OLD.TIPO_NOTA, OLD.id_woocommerce, 'DELETE', now());
END;$$
DELIMITER ;

/**
 * TRIGGERS pedido
 * Logica: monitorar somente se o pedido raiz contiver um id_woocommerce
 */
/*INSERT PEDIDO_LIS*/
DELIMITER $$

CREATE TRIGGER tag_pedido_lis_insert
AFTER INSERT ON pedido_lis
FOR EACH ROW BEGIN
  INSERT INTO modificacoes
    SELECT null, NEW.PEDIDO, 'pedido_lis', 'UPDATE', now(), 0
        FROM DUAL
        WHERE EXISTS (
            SELECT *
            FROM pedido_lis pl
                INNER JOIN pedido p
            ON pl.PEDIDO = p.NUMERO
            WHERE NEW.PEDIDO = p.NUMERO AND p.id_woocommerce IS NOT NULL AND p.id_woocommerce != ''
        );
  INSERT INTO modificacoes 
      SELECT null, CONCAT_WS('.', live.item, il.NUMERO), 'update_qnt', 'UPDATE', now(), 0
      FROM (SELECT NEW.ITEM AS item, NEW.COR AS cor, NEW.TAMANHO AS tam) live
      INNER JOIN item_lis il
        ON il.ITEM=live.item AND il.COR=live.cor AND il.TAMANHO=live.tam
      WHERE EXISTS (
          SELECT *
          FROM pedido_lis pl
              INNER JOIN itens i
          ON pl.ITEM = i.NUMERO
          WHERE NEW.NUMERO = pl.NUMERO AND i.STATUS IS NOT NULL AND i.STATUS != 'INATIV'
      );
  INSERT INTO pedido_lis_bkp VALUES (null, NEW.PEDIDO, NEW.NUMERO, NEW.ITEM, NEW.COR, NEW.TAMANHO, NEW.QTD_INICIAL, NEW.QTD_FINAL, NEW.VLUNITARIO, NEW.ICMS, NEW.ORDEM, 'DELETE', now());
END;$$
DELIMITER ;

/*UPDATE PEDIDO_LIS*/
DELIMITER $$

CREATE TRIGGER tag_pedido_lis_update
AFTER UPDATE ON pedido_lis
FOR EACH ROW BEGIN
  INSERT INTO modificacoes
    SELECT null, NEW.PEDIDO, 'pedido_lis', 'UPDATE', now(), 0
        FROM DUAL
        WHERE EXISTS (
            SELECT *
            FROM pedido_lis pl
                INNER JOIN pedido p
            ON pl.PEDIDO = p.NUMERO
            WHERE NEW.PEDIDO = p.NUMERO AND p.id_woocommerce IS NOT NULL AND p.id_woocommerce != ''
        );
  INSERT INTO modificacoes 
      SELECT null, CONCAT_WS('.', live.item, il.NUMERO), 'update_qnt', 'UPDATE', now(), 0
      FROM (SELECT NEW.ITEM AS item, NEW.COR AS cor, NEW.TAMANHO AS tam) live
      INNER JOIN item_lis il
        ON il.ITEM=live.item AND il.COR=live.cor AND il.TAMANHO=live.tam
      WHERE EXISTS (
          SELECT *
          FROM pedido_lis pl
              INNER JOIN itens i
          ON pl.ITEM = i.NUMERO
          WHERE NEW.NUMERO = pl.NUMERO AND i.STATUS IS NOT NULL AND i.STATUS != 'INATIV'
      );
  INSERT INTO pedido_lis_bkp VALUES (null, NEW.PEDIDO, NEW.NUMERO, NEW.ITEM, NEW.COR, NEW.TAMANHO, NEW.QTD_INICIAL, NEW.QTD_FINAL, NEW.VLUNITARIO, NEW.ICMS, NEW.ORDEM, 'DELETE', now());
END;$$
DELIMITER ;

/*DELETE PEDIDO_LIS*/
DELIMITER $$

CREATE TRIGGER tag_pedido_lis_delete
BEFORE DELETE ON pedido_lis
FOR EACH ROW BEGIN
  INSERT INTO modificacoes
    SELECT null, OLD.PEDIDO, 'pedido_lis', 'UPDATE', now(), 0
        FROM DUAL
        WHERE EXISTS (
            SELECT *
            FROM pedido_lis pl
                INNER JOIN pedido p
            ON pl.PEDIDO = p.NUMERO
            WHERE OLD.PEDIDO = p.NUMERO AND p.id_woocommerce IS NOT NULL AND p.id_woocommerce != ''
        );
  INSERT INTO modificacoes 
      SELECT null, CONCAT_WS('.', live.item, il.NUMERO), 'update_qnt', 'UPDATE', now(), 0
        FROM (SELECT OLD.ITEM AS item, OLD.COR AS cor, OLD.TAMANHO AS tam) live
        INNER JOIN item_lis il
          ON il.ITEM=live.item AND il.COR=live.cor AND il.TAMANHO=live.tam
        WHERE EXISTS (
            SELECT *
            FROM pedido_lis pl
                INNER JOIN itens i
            ON pl.ITEM = i.NUMERO
            WHERE OLD.NUMERO = pl.NUMERO AND i.STATUS IS NOT NULL AND i.STATUS != 'INATIV'
        );
  INSERT INTO pedido_lis_bkp VALUES (null, OLD.PEDIDO, OLD.NUMERO, OLD.ITEM, OLD.COR, OLD.TAMANHO, OLD.QTD_INICIAL, OLD.QTD_FINAL, OLD.VLUNITARIO, OLD.ICMS, OLD.ORDEM, 'DELETE', now());
END;$$
DELIMITER ;


/**
 * TRIGGERS ordem_lis
 * Logica: monitorar quando o valor final alterar. Isto quer dizer que o estoque atualizou
 */
/*INSERT ORDEM_LIS*/
DELIMITER $$

CREATE TRIGGER tag_ordem_lis_insert
AFTER INSERT ON ordem_lis
FOR EACH ROW BEGIN
    IF (NEW.QTD_FINAL > 0) THEN
      INSERT INTO modificacoes 
          SELECT null, CONCAT_WS('.', live.item, il.NUMERO), 'update_qnt', 'UPDATE', now(), 0
          FROM (SELECT NEW.ITEM AS item, NEW.COR AS cor, NEW.TAMANHO AS tam) live
          INNER JOIN item_lis il
            ON il.ITEM=live.item AND il.COR=live.cor AND il.TAMANHO=live.tam
          WHERE EXISTS (
              SELECT *
              FROM ordem_lis ol
                  INNER JOIN itens i
              ON ol.ITEM = i.NUMERO
              WHERE NEW.NUMERO = ol.NUMERO AND i.STATUS IS NOT NULL AND i.STATUS != 'INATIV'
          );
    END IF;
END;$$
DELIMITER ;

/*UPDATE ORDEM_LIS*/
DELIMITER $$

CREATE TRIGGER tag_ordem_lis_update
AFTER UPDATE ON ordem_lis
FOR EACH ROW BEGIN
    IF (NEW.QTD_FINAL > 0) THEN
      INSERT INTO modificacoes 
          SELECT null, CONCAT_WS('.', live.item, il.NUMERO), 'update_qnt', 'UPDATE', now(), 0
          FROM (SELECT NEW.ITEM AS item, NEW.COR AS cor, NEW.TAMANHO AS tam) live
          INNER JOIN item_lis il
            ON il.ITEM=live.item AND il.COR=live.cor AND il.TAMANHO=live.tam
          WHERE EXISTS (
              SELECT *
              FROM ordem_lis ol
                  INNER JOIN itens i
              ON ol.ITEM = i.NUMERO
              WHERE NEW.NUMERO = ol.NUMERO AND i.STATUS IS NOT NULL AND i.STATUS != 'INATIV'
          );
    END IF;
END;$$
DELIMITER ;

/*DELETE ORDEM_LIS*/
DELIMITER $$

CREATE TRIGGER tag_ordem_lis_delete
AFTER UPDATE ON ordem_lis
FOR EACH ROW BEGIN
    IF (OLD.QTD_FINAL > 0) THEN
      INSERT INTO modificacoes 
          SELECT null, CONCAT_WS('.', live.item, il.NUMERO), 'update_qnt', 'UPDATE', now(), 0
          FROM (SELECT OLD.ITEM AS item, OLD.COR AS cor, OLD.TAMANHO AS tam) live
          INNER JOIN item_lis il
            ON il.ITEM=live.item AND il.COR=live.cor AND il.TAMANHO=live.tam
          WHERE EXISTS (
              SELECT *
              FROM ordem_lis ol
                  INNER JOIN itens i
              ON ol.ITEM = i.NUMERO
              WHERE OLD.NUMERO = ol.NUMERO AND i.STATUS IS NOT NULL AND i.STATUS != 'INATIV'
          );
    END IF;
END;$$
DELIMITER ;