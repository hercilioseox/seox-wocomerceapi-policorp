<?php 
include 'conex.php';

$data = [
    'product' => [
        'title' => 'Premium Quality',
        'type' => 'simple',
        'regular_price' => '21.99',
        'description' => 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.',
        'short_description' => 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.',
        'categories' => [
            9,
            14
        ],
        'images' => [
            [
                'src' => 'http://example.com/wp-content/uploads/2015/01/premium-quality-front.jpg',
                'position' => 0
            ],
            [
                'src' => 'http://example.com/wp-content/uploads/2015/01/premium-quality-back.jpg',
                'position' => 1
            ]
        ]
    ]
];
echo '<pre>';
print_r($woocommerce->post('products', $data));

?>