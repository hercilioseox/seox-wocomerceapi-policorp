<?php 
function Render_template_sign_up_confirmation($first_name, $username, $email, $password) {
    $html = '
        <!DOCTYPE html>
        <html>
        <head>
            <style>
                .selection:hover + .selection-hover {
                    display: inline;
                }
                
                .selection:active + .selection-hover {
                    display: none;
                }
                
                .selection-hover {
                    display: none;
                    background: rgb(36, 180, 111);
                    width: 50px;     
                    color: white;
                    padding: 3px 4px 3px 4px;
                    margin-left: 3px;
                    border-radius: 4px;
                }        
            </style>
        </head>
        <body>
            <div id="wrapper" dir="ltr" style="background-color: #f7f7f7; margin: 0; padding: 70px 0 70px 0; width: 100%; -webkit-text-size-adjust: none;">
                <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
                    <tbody>
                        <tr>
                            <td align="center" valign="top">
                                <div id="template_header_image"></div>
                                <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_container" style="background-color: #ffffff; border: 1px solid #dedede; box-shadow: 0 1px 4px rgba(0,0,0,0.1); border-radius: 3px;">
                                    <tbody>
                                        <tr>
                                            <td align="center" valign="top">
                                                <!-- Header -->
                                                <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_header" style="background-color: #24b46f; color: #ffffff; border-bottom: 0; font-weight: bold; line-height: 100%; vertical-align: middle; font-family: &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; border-radius: 3px 3px 0 0;">
                                                    <tbody>
                                                        <tr>
                                                            <td id="header_wrapper" style="padding: 36px 48px; display: block;">
                                                                <h1 style="color: #ffffff; font-family: &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; font-size: 30px; font-weight: 300; line-height: 150%; margin: 0; text-align: left; text-shadow: 0 1px 0 #50c38c;">Bem Vindo a Policorp</h1>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!-- End Header -->
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top">
                                                <!-- Body -->
                                                <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_body">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="top" id="body_content" style="background-color: #ffffff;">
                                                                <!-- Content -->
                                                                <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td valign="top" style="padding: 48px 48px 0;">
                                                                                <div id="body_content_inner" style="color: #636363; font-family: &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;">
                                                                                    <div id="m_-2670289985160498033body_content_inner" style="color:#636363;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:14px;line-height:150%;text-align:left">
                                                                                        <p style="margin:0 0 16px"><strong>Olá '.$first_name.', você foi cadastrado com exito no site para Revendedores da Policorp.</strong></p>
                                                                                        <table id="m_-2670289985160498033m_-3379090805971024574addresses" cellspacing="0" cellpadding="0" border="0" style="width:100%;vertical-align:top;margin-bottom:40px;padding:0">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td valign="top" width="50%" style="padding:0;text-align:left;font-family:" Helvetica Neue",Helvetica,Roboto,Arial,sans-serif;border:0">
                                                                                                        <h2 style="color:#24b46f;display:block;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:18px;font-weight:bold;line-height:130%;margin:0 0 18px;text-align:left">Seu Acesso ao Site</h2>
                                                                                                        <address class="m_-2670289985160498033m_-7547331375339303472address" style="padding:12px;color:#636363;border:1px solid #e5e5e5">
                                                                                                            <!-- Variaveis vao aqui -->
                                                                                                            <strong>Link de acesso: </strong><a href="policorp.seox.com.br/minha-conta" style="color: #24b46f;">policorp.seox.com.br</a>
                                                                                                            <div>
                                                                                                            <strong>Nome de usuário: </strong><span id="username" class="selection">'.$username.'</span>
                                                                                                            <span class="selection-hover">Copiar</span>
                                                                                                            <br>
                                                                                                            <strong>Senha: </strong><span id="password" class="selection">'.$password.'</span>
                                                                                                            <span class="selection-hover">Copiar</span>
                                                                                                            <br>
                                                                                                            <strong>Email: </strong><a href="mailto:'.$email.'" style="color:#24b46f;font-weight:normal;text-decoration:underline" target="_blank">'.$email.'</a>
                                                                                                            <br>
                                                                                                            <span class="m_-2670289985160498033HOEnZb">
                                                                                                                <font color="#888888"></font>
                                                                                                            </span>
                                                                                                        </address>
                                                                                                        <span class="m_-2670289985160498033HOEnZb">
                                                                                                            <font color="#888888"></font>
                                                                                                        </span>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                        <p style="margin:0 0 16px">Obrigado!</p>
                                                                                        <span class="HOEnZb">
                                                                                            <font color="#888888"></font>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <!-- End Content -->
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!-- End Body -->
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top">
                                                <!-- Footer -->
                                                <table border="0" cellpadding="10" cellspacing="0" width="600" id="template_footer">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="top" style="padding: 0; -webkit-border-radius: 6px;">
                                                                <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td colspan="2" valign="middle" id="credit" style="padding: 0 48px 48px 48px; -webkit-border-radius: 6px; border: 0; color: #7cd2a9; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;">
                                                                                <a href="policorp.seox.com.br" style="color: #7cd2a9;">
                                                                                    <p>Policorp</p>
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!-- End Footer -->
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <script>
                document.getElementById("username").addEventListener("click", function() {
                    copy_password("username");
                });
                document.getElementById("password").addEventListener("click", function() {
                    copy_password("password");
                });

                function copy_password(reference) {
                    var copyText = document.getElementById(reference);
                    var textArea = document.createElement("textarea");
                    textArea.value = copyText.textContent;
                    document.body.appendChild(textArea);
                    textArea.select();
                    document.execCommand("Copy");
                    textArea.remove();
                    document.getElementById("username-content").innerHTML = "Copiado";
                }
            </script>
        </body>
        </html>
    ';
    return $html;
}
 
function Render_template_sign_up_failed($first_name, $last_name, $email, $id_caravell) {
    $html = '
        <div id="wrapper" dir="ltr" style="background-color: #f7f7f7; margin: 0; padding: 70px 0 70px 0; width: 100%; -webkit-text-size-adjust: none;">
            <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
                <tbody>
                    <tr>
                        <td align="center" valign="top">
                            <div id="template_header_image"></div>
                            <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_container" style="background-color: #ffffff; border: 1px solid #dedede; box-shadow: 0 1px 4px rgba(0,0,0,0.1); border-radius: 3px;">
                                <tbody>
                                    <tr>
                                        <td align="center" valign="top">
                                            <!-- Header -->
                                            <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_header" style="background-color: #ff0000; color: #ffffff; border-bottom: 0; font-weight: bold; line-height: 100%; vertical-align: middle; font-family: &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; border-radius: 3px 3px 0 0;">
                                                <tbody>
                                                    <tr>
                                                        <td id="header_wrapper" style="padding: 36px 48px; display: block;">
                                                            <h1 style="color: #ffffff; font-family: &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; font-size: 30px; font-weight: 300; line-height: 150%; margin: 0; text-align: left; text-shadow: 0 1px 0 #50c38c;">Revendedor Não Cadastrado</h1>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        <!-- End Header -->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top">
                                            <!-- Body -->
                                            <table border="0" cellpadding="0" cellspacing="0" width="600" id="template_body">
                                                <tbody>
                                                    <tr>
                                                        <td valign="top" id="body_content" style="background-color: #ffffff;">
                                                            <!-- Content -->
                                                            <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                                <tbody>
                                                                    <tr>
                                                                        <td valign="top" style="padding: 48px 48px 0;">
                                                                            <div id="body_content_inner" style="color: #636363; font-family: &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; font-size: 14px; line-height: 150%; text-align: left;">
                                                                                <div id="m_-2670289985160498033body_content_inner" style="color:#636363;font-family:&quot;Helvetica Neue&quot;,Helvetica,Roboto,Arial,sans-serif;font-size:14px;line-height:150%;text-align:left">
                                                                                    <p style="margin:0 0 16px"><strong>Revendedor '.$first_name.' '.$last_name.' (ip: #'.$id_caravell.') não pode ser inserido no site para revendedores.</strong></p>
                                                                                    <p style="margin:0 0 16px">Por favor verificar email cadastrado no Caravella, pois o mesmo pode já ter sido inserido no site ou ser inválido.</p>
                                                                                        <table id="m_-2670289985160498033m_-3379090805971024574addresses" cellspacing="0" cellpadding="0" border="0" style="width:100%;vertical-align:top;margin-bottom:40px;padding:0">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td valign="top" width="50%" style="padding:0;text-align:left;font-family:"Helvetica Neue",Helvetica,Roboto,Arial,sans-serif;border:0">
                                                                                                        <address class="m_-2670289985160498033m_-7547331375339303472address" style="padding:12px;color:#636363;border:1px solid #e5e5e5">
                                                                                                            <strong>Email:  </strong><a href="mailto:'.$email.'" style="color:#ff0000;font-weight:normal;text-decoration:underline" target="_blank">'.$email.'</a>
                                                                                                            <span class="m_-2670289985160498033HOEnZb"><font color="#888888"></font></span>
                                                                                                        </address>
                                                                                                        <span class="m_-2670289985160498033HOEnZb"><font color="#888888"></font></span>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    <p style="margin:0 0 16px">Obrigado!</p>
                                                                                    <span class="HOEnZb"><font color="#888888"></font></span>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        <!-- End Content -->
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        <!-- End Body -->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top">
                                            <!-- Footer -->
                                            <table border="0" cellpadding="10" cellspacing="0" width="600" id="template_footer">
                                                <tbody>
                                                    <tr>
                                                        <td valign="top" style="padding: 0; -webkit-border-radius: 6px;">
                                                            <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                                                <tbody>
                                                                    <tr>
                                                                        <td colspan="2" valign="middle" id="credit" style="padding: 0 48px 48px 48px; -webkit-border-radius: 6px; border: 0; color: #ff0000; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;">
                                                                            <a href="policorp.seox.com.br" style="color: #ff0000;"><p>Policorp</p></a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        <!-- End Footer -->
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    ';
    return $html;
}
