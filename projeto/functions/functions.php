<?php
/***************************
 * FUNCOES API WOOCOMMERCE *
 ***************************/

/** 
 * CRUD BASIC WOOCOMMERCE API 
 * 
 * Discionario para parametro $tabela:
 * - Usuarios       => customers
 * - Produtos       => products
 * - Categorias     => products/categories
 * - Pedidos        => orders
 * - variacoes      => products/id_produto/variations
 * - atributos     => products/attributes
 * 
 * @func1: Inserir_wooAPI
 * @func2: Atualizar_wooAPI
 * @func3: Buscar_wooAPI
 * @func4: Buscar_id_wooAPI
 * $func5: Verificar_wooAPI
 * @func6: Deletar_wooAPI
 * @func7: Batch_wooAPI
 */
function Inserir_wooAPI($address, $data) {
    include 'conex_wooapi.php';

    try {
        $result = $woocommerce->post($address, $data);
        return $result;
    } catch (Exception $e) {
        Logging($funcao='Inserir_wooAPI : '.$address, $status=$e->getMessage());
    }
}

function Atualizar_wooAPI($address, $data) {
    include 'conex_wooapi.php';

    try {
        $result = $woocommerce->put($address, $data);
        return $result;
    } catch (Exception $e) {
        Logging($funcao='Atualizar_wooAPI : '.$address, $status=$e->getMessage());
    }
}

function Buscar_wooAPI($address, $page=1) {
    include 'conex_wooapi.php';

    try {
        $result = $woocommerce->get($address, array('page' => $page, 'per_page' => 99));
        return $result;
    } catch (Exception $e) {
        Logging($funcao='Buscar_wooAPI : '.$address, $status='erro');
    }
}

function Buscar_id_wooAPI($address, $id) {
    include 'conex_wooapi.php';

    try {
        $result = $woocommerce->get($address.'/'.$id);
        return $result;
    } catch (Exception $e) {
        Logging($funcao='Buscar_id_wooAPI : '.$address, $status='erro');
    }
}

function Verificar_wooAPI($address, $id) {
    include 'conex_wooapi.php';
    
    try {
        $result = $woocommerce->get($address.'/'.$id);
        if ($result != null) {
            return true; 
        }
        return false;
    } catch (Exception $e) {
        return false;
    } 
}

function Deletar_wooAPI($address) {
    include 'conex_wooapi.php';

    try {
        $result = $woocommerce->delete($address, ['force' => true]);
        return $result;
    } catch (Exception $e) {
        Logging($funcao='Deletar_wooAPI : '.$address, $status=$e->getMessage());
    }
}

function Batch_wooAPI($address, $data) {
    set_time_limit(0);
    include 'conex_wooapi.php';

    try {
        $result = $woocommerce->post($address.'/batch', $data);
        return $result;
    } catch (Exception $e) {
        Logging($funcao='Batch_wooAPI : '.$address, $status=$e->getMessage());
    }
}

/**
 * CRUO: PRODUTOS
 * 
 * @func1: Buscar_id_pelo_sku_wooAPI
 * @func2: Buscar_produto_sku_wooAPI
 * @func3: Deletar_produto_wooAPI
 * @func4: Inserir_novo_produto_wooAPI
 * @func5: Atualizar_taxonomia_de_um_produto_wpAPi
 * @func6: Inserir_todos_produtos_disponiveis
 * @func7: Deletar_todos_produtos_wooAPI
 */
// GET id com base no sku
function Buscar_id_pelo_sku_wooAPI(string $sku) {
    $prod = Buscar_produto_sku_wooAPI($sku);
    return $prod[0]->id;
}

// GET produto com base no sku
function Buscar_produto_sku_wooAPI(string $sku) {
    include 'conex_wooapi.php';
    
    try {
        $result = $woocommerce->get('products', array('sku' => $sku));
        return $result;
    } catch (Exception $e) {
        Logging($funcao='Buscar_produto_sku_wooAPI', $status='erro');
    } 
}

// DELETE produto com base no sku
function Deletar_produto_wooAPI($sku_produto) {    
    $prod_id = Buscar_produto_sku_wooAPI($sku_produto);
    if ($prod_id != null) {
        $result = Deletar_wooAPI('products/'.$prod_id[0]->id);
        return $result;
    } else {
        Logging($funcao='Deletar_produto_wooAPI. Sku: '.$sku_produto, $status='erro: produto nao estava inserido no site');
        return 3;
    }
}

// Inserir um produto via woocommerce e atualizar suas taxonomias se necessario
function Inserir_novo_produto_wooAPI($produto, $is_update=false, $prod_id=null) {
    // Guarda as taxonomias em variaveis auxiliares
    $destaque_promocao_outlet = null;
    $tipos_de_roupas = null;
    $colecao = null;

    if (array_key_exists('destaque_promocao_outlet', $produto)) {
        $destaque_promocao_outlet = $produto['destaque_promocao_outlet'];
        unset($produto['destaque_promocao_outlet']);
    }
    if (array_key_exists('tipos_de_roupas', $produto)) {
        $tipos_de_roupas = $produto['tipos_de_roupas'];
        unset($produto['tipos_de_roupas']);
    }
    if (array_key_exists('colecao', $produto)) {
        $colecao = $produto['colecao'];
        unset($produto['colecao']);
    }

    try {
        if ($is_update) {
            if ($prod_id == null) {
                $prod_id = Buscar_id_pelo_sku_wooAPI(strval($produto['sku']));
            }
            $response_api = Atualizar_wooAPI('products/'.$prod_id, $produto);
        } else {
            $response_api = Inserir_wooAPI('products', $produto);
        }
    } catch (Exception $e) {
        Logging($funcao='Inserir_novo_produto_wooAPI. Ip_woo: '.$produto['id'], $status=$e->getMessage());
    }
    if(!$response_api) {
        return;
    }

    // Prepara array no formato JSON
    $params = array();
    if ($destaque_promocao_outlet != null && count($destaque_promocao_outlet) > 0) {
        $params['destaque_promocao_outlet'] = $destaque_promocao_outlet;
    }
    if ($tipos_de_roupas != null  && count($tipos_de_roupas) > 0) {
        $params['tipos_de_roupas'] = $tipos_de_roupas;
    }
    if ($colecao != null && count($colecao) > 0) {
        $params['colecao'] = $colecao;
    }

    //Se tem taxonomia atauliza wp APi
    if (count($params) > 0) {
        $response_api = Atualizar_taxonomia_de_um_produto_wpAPi($response_api->id, $params);
    }
    // Caso não haja taxonomia retorna a resposta do woocommerce
    return $response_api;
}

// Atualiza taxonomias utilizando wpAPI
function Atualizar_taxonomia_de_um_produto_wpAPi($id_produto, $taxonomias) {
    try {
        include 'conex_wpapi.php';

        $response = $wordpress->put('wp-json/wp/v2/product/'.$id_produto, array('query' => $taxonomias));
        return json_decode($response->getBody());
    } catch (Exception $e) {
        Logging($funcao='Atualizar_taxonomia_de_um_produto_wpAPi. Ip_woo: '.$id_produto, $status=$e->getMessage());
    }
}

// Inserir todos os produtos do banco no woocommerce e worpress
function Inserir_todos_produtos_disponiveis($insert_unico_sku=null, $is_update=false) {
    // Colecao
    $colecao = Buscar_taxonomia_wpAPI($taxo_address='colecao/?per_page=99');
    $ids_colecao = [];
    foreach ($colecao as $colecao_row) {
        $ids_colecao[$colecao_row->name] = $colecao_row->id;
    }

    // Tipos de roupas
    $tip_roupas = Buscar_taxonomia_wpAPI($taxo_address='tipos_de_roupas/?per_page=99');
    $ids_tip_roupas = [];
    foreach ($tip_roupas as $tip_roupas_row) {
        $ids_tip_roupas[$tip_roupas_row->name] = $tip_roupas_row->id;
    }

    // Em destaque / Em promocao
    $destaq_promo_out = Buscar_taxonomia_wpAPI($taxo_address='destaque_promocao_outlet/?per_page=99');
    $ids_destaq_promo_out = [];
    foreach ($destaq_promo_out as $destaq_promo_out_row) {
        $ids_destaq_promo_out[$destaq_promo_out_row->slug] = $destaq_promo_out_row->id;
    }

    // Em destaque / Em promocao
    $categories = Buscar_wooAPI('products/categories');
    $ids_categories = [];
    foreach ($categories as $categories_row) {
        $ids_categories[$categories_row->slug] = $categories_row->id;
    }

    $sql = "SELECT i.NUMERO
                , i.NOME
                , i.VLVENDA3
                , i.MASCUL
                , i.FEMINI
                , i.INFANT 
                , i.PROMO
                , i.DESTAQ
                , i.LINHA
                , lin.NOME
                , i.COLECAO
                , col.NOME
                , i.STATUS
                , i.OUTLET
                , i.DESCONTO_PROMO
            FROM itens i
                LEFT JOIN item_linha lin
            ON i.LINHA = lin.NUMERO
                LEFT JOIN item_colecao col
            ON i.COLECAO = col.NUMERO";

    if ($insert_unico_sku != null) {
        $sql .= " WHERE i.NUMERO = ".$insert_unico_sku.";";
    } else {
        //ISSUE: A ser removido na versao final
        $sql .= " WHERE i.STATUS IS NOT NULL AND i.STATUS != 'INATIV' ORDER BY NUMERO ASC;";
    }
    $result = Enviar_sql_bd($sql);
    
    if (count($result) <= 0) {
        if ($insert_unico_sku != null) {
            Logging($funcao='Inserir_todos_produtos_disponiveis. Sku: '.$insert_unico_sku, $status='erro: produto nao exite no banco de dados.');
        } else {
            Logging($funcao='Inserir_todos_produtos_disponiveis', $status='erro: banco vazio.');
        }
        return 2;
    }

    # Linha=[9] e Colecao=[11]
    foreach ($result as $result_row) {

        /*                              PRODUC      | ESTOQU    | CONSUL        | INATIV
            [status]                => publish      | publish   | publish       | draft
            [manage_stock]          =>              | true      | true          |
            [stock_quantity]        =>              |           | 0             |
            [stock_status]          => instock      | instock   | onbackorder   | outofstock
            [backorders]            => no           | no        | notify        | no
            [backorders_allowed]    =>              |           | true          |
            [backordered]           =>              |           | true          |
        */
        // Padrao eh inativo
        $data = [
            'name'                       => $result_row['1']
            , 'sku'                      => $result_row['NUMERO']
            , 'type'                     => 'variable'
            , 'status'                   => 'draft'
            , 'sale_price'               => null
            , 'manage_stock'             => null
            , 'stock_quantity'           => null
            , 'stock_status'             => 'outofstock'
            , 'backorders'               => 'no'
            , 'backorders_allowed'       => null
            , 'backordered'              => null
            , 'categories'               => []
            , 'destaque_promocao_outlet' => [-1]
            , 'tipos_de_roupas'          => [-1]
            , 'colecao'                  => [-1]
        ];

        //status
        // if ($result_row['STATUS'] == 'CONSUL') {
        //     $data['status']             = 'publish';
        //     $data['manage_stock']       = true;
        //     $data['stock_quantity']     = '0';
        //     $data['stock_status']       = 'onbackorder';
        //     $data['backorders']         = 'notify';
        //     $data['backorders_allowed'] = true;
        //     $data['backordered']        = true;
        // } 
        
        if ($result_row['STATUS'] == 'PRODUC' || $result_row['STATUS'] == 'ESTOQU') {
            $data['status'] = 'publish';
        }

        // categorias
        if (strcasecmp($result_row['MASCUL'], 'S') == 0) {
            $data['categories'][] = ['id' => $ids_categories['masculina']];
        }
        if (strcasecmp($result_row['FEMINI'], 'S') == 0) {
            $data['categories'][] = ['id' => $ids_categories['feminina']];
        }
        if (strcasecmp($result_row['INFANT'], 'S') == 0) {
            $data['categories'][] = ['id' => $ids_categories['infantil']];
        }

        // em destaque em promocao
        if (strcasecmp($result_row['PROMO'], 'S') == 0) {
            $data['destaque_promocao_outlet'][] = $ids_destaq_promo_out['em_promocao'];
        }
        if (strcasecmp($result_row['DESTAQ'], 'S') == 0) {
            $data['destaque_promocao_outlet'][] = $ids_destaq_promo_out['em_destaque'];
        }
        if (strcasecmp($result_row['OUTLET'], 'S') == 0) {
            $data['destaque_promocao_outlet'][] = $ids_destaq_promo_out['em_outlet'];
        }

        // tipos de roupas
        if($result_row['LINHA'] != null) {
            $data['tipos_de_roupas'] = [$ids_tip_roupas[$result_row[9]]];
        }

        // colecao
        if($result_row['COLECAO'] != null) {
            $data['colecao'] = [$ids_colecao[$result_row[11]]];
        }

        // Verifica se update ou um insert unico pois sendo assim posso retornar algum valor
        if ($is_update || $insert_unico_sku != null) {
            // Caso seja um update
            if ($is_update) {
                $prod = Buscar_produto_sku_wooAPI($insert_unico_sku);
                // Caso o produto esteja com status nulo
                if ($prod == null) {
                    $new_prod = Inserir_novo_produto_wooAPI($data, false);
                    $sql = "SELECT * FROM item_lis 
                        WHERE ITEM = ".$insert_unico_sku.";";
                    $variations = Enviar_sql_bd($sql);
                    foreach ($variations as $row) {
                        Inserir_atualizar_variacao_registrada_wooAPI($insert_unico_sku.'-'.$row['NUMERO']);
                    }
                    return $new_prod;
                }

                $new_sale_price = null;
                $new_price = null;
                // caso esteja em promocao 
                if (strcasecmp($result_row['PROMO'], 'S') == 0) {
                    $percent = intval($result_row['DESCONTO_PROMO']);
                    $price = intval($result_row['VLVENDA3']);
                    if ($percent != null) {
                        $new_sale_price = $price - (($percent/100) * $price);
                    }
                } else if ($prod[0]->price < intval($result_row['VLVENDA3'])) {
                    $new_sale_price = -1;
                }
                // caso o preco tenha mudado
                if ($prod[0]->price != $result_row['VLVENDA3']) {
                    $new_price = $result_row['VLVENDA3'];
                } 
                Atualizar_precos_status_de_variacoes_wooAPI($prod[0], $new_price, $new_sale_price, $result_row['STATUS']);                  
                
                $result = Inserir_novo_produto_wooAPI($data, $is_update, $prod[0]->id);
                // tratamento de erro
                if ($result != null) {
                    return $result;
                } 
                Logging($funcao='Inserir_todos_produtos_disponiveis. Sku: '.$insert_unico_sku, $status='erro: erro ao tentar atualizar produto.');
                return 3;
            }
            $result = Inserir_novo_produto_wooAPI($data, false);
            if ($result != null) {
                $sql = "SELECT * FROM item_lis 
                WHERE ITEM = ".$insert_unico_sku.";";
                $variations = Enviar_sql_bd($sql);
                foreach ($variations as $row) {
                    Inserir_atualizar_variacao_registrada_wooAPI($insert_unico_sku.'-'.$row['NUMERO']);
                }
                return $result;
            }
            Logging($funcao='Inserir_todos_produtos_disponiveis. Sku: '.$insert_unico_sku, $status='erro: erro ao tentar inserir produto unico.');
            return 3;
        } else {
            Inserir_novo_produto_wooAPI($data, $is_update);
            echo '<pre>';
            print_r($data);
        }
    }
}

function Deletar_todos_produtos_wooAPI() {
    while (true) {
        $data = ['delete' => []];
        $pro = Buscar_wooAPI('products');
        if(count($pro) == 0) {
            return true;
        }
        foreach ($pro as $key => $row) {
            $data['delete'][$key] = $row->id;
        }
        Batch_wooAPI('products', $data);
    }
}

/**
 * CRUD: ATRIBUTOS de um produto
 * https://florianbrinkmann.com/en/4526/creating-a-woocommerce-product-variation-rest-api/
 * 
 * @func1: Inserir_atributos_em_um_produto_wooAPI
 * @func2: Atualizar_atributos_em_um_produto_wooAPI
 * @func3: Inserir_todos_atributos_de_produtos_disponiveis
 * @func4: Buscar_produto_atributos_wooAPI
 * $func5: Buscar_cor_tecido_id_pelo_nome_wooAPI
 */
// Funcao Inserir e Atualizar
function Inserir_atributos_em_um_produto_wooAPI($id_produto, $data) {
    $data_end = ['attributes' => $data];
    $address = 'products/'.$id_produto;
    return Inserir_wooAPI($address, $data_end);
}

function Atualizar_atributos_em_um_produto_wooAPI($id_produto, $atributos) {
    $data = ['attributes' => $atributos];
    $address = 'products/'.$id_produto;
    return Atualizar_wooAPI($address, $data);
}

// Inserir todos os atributos de produtos do banco no woocommerce e worpress
function Inserir_todos_atributos_de_produtos_disponiveis() {
    // Cria a categoria de tributo
    $data_batch = [
        'create' => [
            [
                'name' => 'Tamanho'
                , 'type' => 'button'
            ],
            [   
                'name' => 'Cor / Tecido'
                , 'type' => 'color'
            ]
        ]
    ];
    $batch_result = Batch_wooAPI('products/attributes', $data_batch);

    // Incia processo para adiconar um atributo em cada produto
    $sku_produtos = Selecionar_itens_not_null_bd();

    $data = ['update' => []];
    $index = 0;
    foreach ($sku_produtos as $sku_row) {
        $prod_id = Buscar_id_pelo_sku_wooAPI(strval($sku_row[0]));

        $sql = "SELECT lis.ITEM
                    , lis.NUMERO
                    , lis.TAMANHO
                    , lis.COR
                    , c.NOME
                    , lis.QTD
                FROM item_lis lis
                    LEFT JOIN cores c
                ON lis.COR = c.NUMERO
                WHERE lis.ITEM = ".$sku_row[0]."";

        $atributos = Enviar_sql_bd($sql);

        if (count($atributos) <= 0) {
            continue;
        }
        
        // utilizacao de sets para atributos sem repeticao
        $tamanho = $cor = [];
        // guarda todos os atributos
        $options_cor = $options_tam = [];
        foreach ($atributos as $att_row) {
            if ($att_row['TAMANHO'] != null) {
                $tamanho[$att_row['TAMANHO']] = $att_row['TAMANHO'];
            }
            
            if ($att_row['NOME'] != null) {
                $cor[$att_row['NOME']] = $att_row['NOME'];
            }
        }
        foreach ($tamanho as $row) {
            $options_tam[] = $row;
        }
        foreach ($cor as $row) { 
            $options_cor[] = $row;
        }
        
        // define qual produto vai receber os atributos
        $data['update'][$index]['id'] = $prod_id;
        // setta tamanho
        if ($options_tam != null && count($options_tam) > 0) {
            $data['update'][$index]['attributes'][] = [
                'id' => $batch_result->create[0]->id,
                'visible' => true,
                'variation' => true, 
                'options' => $options_tam
            ];
        }
        // setta cor
        if ($options_cor != null && count($options_cor) > 0) {
            $data['update'][$index]['attributes'][] = [
                'id' => $batch_result->create[1]->id,
                'visible' => true,
                'variation' => true, 
                'options' => $options_cor
            ];
        }

        if($index == 99) {        
            Batch_wooAPI('products', $data);
            $data = ['update' => []];
            $index = 0;
        } else {
            $index++;
        }
    }

    Batch_wooAPI('products', $data);
}

function Buscar_produto_atributos_wooAPI() {
    include 'conex_wooapi.php';

    try {
        $result = $woocommerce->get('products/attributes/2/terms');
        return $result;
    } catch (Exception $e) {
        Logging($funcao='Buscar_produto_atributos_wooAPI', $status='erro');
    }
}

// Utilziado para buscar um id pelo nome da cor / tecido
function Buscar_cor_tecido_id_pelo_nome_wooAPI($nome, $atribute_id) {
    $index = 1;
    while (true) {
        $cores = Buscar_wooAPI('products/attributes/'.$atribute_id.'/terms', $index);
        if (count($cores) == 0 || $cores == null) {
            break;
        }
        foreach ($cores as $row) {
            if ($row->name == $nome) {
                return $row;
            }
        }
        $index++;
    }
}

/**
 * CRUD: VARIACOES de um Produto
 * 
 * @func1: Inserir_todas_variacoes_de_produtos_disponiveis
 * @func2: Inserir_atualizar_variacao_registrada_wooAPI
 * @func3: Mapear_variacao_data_para_insercao_update_wooAPI
 * @func4: Deletar_varicao_wooAPI
 * @func5: Atualizar_precos_status_de_variacoes_wooAPI
 * @func6: Acessar_quantidade_do_produto
 */
// INSERT todas as variacoes de produtos do banco no woocommerce e worpress
function Inserir_todas_variacoes_de_produtos_disponiveis() {
    // utilizado para pegar a sku de produtos
    $sku_produtos = Selecionar_itens_not_null_bd();
    // utilizado para pegar o id de atributos
    $id_atributos = Buscar_wooAPI('products/attributes');

    foreach ($sku_produtos as $sku_row) {
        $prod_id = Buscar_id_pelo_sku_wooAPI(strval($sku_row[0]));
        if($prod_id == null) {
            Logging($funcao='Inserir_todas_variacoes_de_produtos_disponiveis. Sku: '.$sku_row[0], $status='erro: produto raiz nao foi inserido no banco.');
            continue;
        }

        $sql = "SELECT lis.ITEM
                , lis.NUMERO
                , lis.TAMANHO
                , lis.COR
                , c.NOME
                , lis.QTD
                , i.VLVENDA3
            FROM item_lis lis
                INNER JOIN cores c
            ON lis.COR = c.NUMERO
                INNER JOIN itens i
            ON lis.ITEM = i.NUMERO
            WHERE lis.ITEM = ".$sku_row[0].";";
            
        $variacoes = Enviar_sql_bd($sql);

        if (count($variacoes) <= 0) {
            continue;
        }

        $data_end = ['create' => []];
        $index = 0;
        foreach ($variacoes as $var_row) {
            try {
                $data_end['create'][$index] = Mapear_variacao_data_para_insercao_update_wooAPI($id_atributos, $sku_row, $var_row);
            } catch (Exception $e) {
                Logging($funcao='Inserir_todas_variacoes_de_produtos_disponiveis', $status='erro: ao mapear.');
                continue;
            }

            if($index == 99) {
                try {
                    Batch_wooAPI('products/'.$prod_id.'/variations', $data_end);
                } catch (Exception $e) {
                    Logging($funcao='Inserir_todas_variacoes_de_produtos_disponiveis - sku: '.$sku_row, $status=$e->getMessage());
                }
                
                $data_end = ['create' => []];
                $index = 0;
            } else {
                $index++;
            }
        }
        Batch_wooAPI('products/'.$prod_id.'/variations', $data_end);
    }
}

// INSERT/UPDATE variacao unica
function Inserir_atualizar_variacao_registrada_wooAPI($insert_unico_sku, $is_update=false) {

    $sku_splited = explode("-", $insert_unico_sku);
    $item_sku = $sku_splited[0];
    $numero_sku = $sku_splited[1];

    // utilizado para pegar o id de um produto
    $prod_id = Buscar_id_pelo_sku_wooAPI(strval($item_sku));
    // Utilizado para pegar o status de um produto
    $root = Selecionar_id_bd('itens', $item_sku);
    // utilizado para pegar o id de atributos
    $id_atributos = Buscar_wooAPI('products/attributes');

    $sql = "SELECT lis.ITEM
            , lis.NUMERO
            , lis.TAMANHO
            , lis.COR
            , c.NOME
            , lis.QTD
            , i.VLVENDA3
        FROM item_lis lis
            LEFT JOIN cores c
        ON lis.COR = c.NUMERO
            INNER JOIN itens i
        ON lis.ITEM = i.NUMERO
        WHERE lis.ITEM = ".$item_sku." and lis.NUMERO = ".$numero_sku.";";
    $variacao = Enviar_sql_bd($sql);

    if (count($variacao) <= 0) {
        Logging($funcao='Inserir_atualizar_variacao_registrada_wooAPI. Sku: '.$insert_unico_sku, $status='erro: variacoes de ('.$insert_unico_sku.') nao exite no banco de dados.');
        return 2;
    }

    if($variacao[0]['TAMANHO'] == null || ($variacao[0]['COR'] == null || $variacao[0]['NOME'] == null)) {
        Logging($funcao='Inserir_atualizar_variacao_registrada_wooAPI. Sku: '.$insert_unico_sku, $status='erro: algum atributo nulo.');
        return 3;
    }

    try {     
        $root_att = Buscar_id_wooAPI('products', $prod_id)->attributes;
        // Caso nao haja nenhum atributo neste produto
        if (count($root_att) <= 0) {
            $root_att = [
                1 => [
                    'id'        => $id_atributos[1]->id,
                    'visible'   => true, 
                    'variation' => true,
                    'options'   => [$variacao[0]['TAMANHO']]
                ],
                0 => [
                    'id'        => $id_atributos[0]->id,
                    'visible'   => true, 
                    'variation' => true,
                    'options'   => [$variacao[0]['NOME']]
                ]
            ];
            Inserir_atributos_em_um_produto_wooAPI($prod_id, $root_att);
        }
        // Se houver atributos ele insere um novo (soh ira inserir o atributo se este for novo - woocommerce configuration) 
        else {
            if ($root_att[0]->name == 'Tamanho') {
                $root_att[0]->options[] = $variacao[0]['TAMANHO'];
                $root_att[1]->options[] = $variacao[0]['NOME'];
            } else {
                $root_att[0]->options[] = $variacao[0]['NOME'];
                $root_att[1]->options[] = $variacao[0]['TAMANHO'];
            }

            // Atualiza atributos
            Atualizar_atributos_em_um_produto_wooAPI($prod_id, $root_att);
        }
    } catch (Exception $e) {
        Logging($funcao='Inserir_atualizar_variacao_registrada_wooAPI. Sku: '.$insert_unico_sku, $status='erro: erro ao tentar atualizar atributos.');
        return 3;
    }
    
    $data = Mapear_variacao_data_para_insercao_update_wooAPI($id_atributos, $root[0], $variacao[0]);

    if ($is_update) {
        $var_id = Buscar_id_pelo_sku_wooAPI(strval($insert_unico_sku));
        $result = Atualizar_wooAPI('products/'.$prod_id.'/variations/'.$var_id, $data);
    } else {
        $result = Inserir_wooAPI('products/'.$prod_id.'/variations', $data);
    }
    if ($result != null) {
        return $result;
    } else {
        Logging($funcao='Inserir_atualizar_variacao_registrada_wooAPI. Sku: '.$insert_unico_sku, $status='erro: ao tentar inserir/atualizar variacao.');
        return 3;
    }
}

// Cria json para insercao/atualizacao de uma variacao
function Mapear_variacao_data_para_insercao_update_wooAPI($id_atributos, $root, $var_row) {

    $data = [];
    // setta sku
    $data['sku'] = $var_row['ITEM']."-".$var_row['NUMERO'];
    // setta price
    $data['regular_price'] = $var_row['VLVENDA3'];
    // sale price
    if (strcasecmp($root['PROMO'], 'S') == 0) {
        $percent = intval($root['DESCONTO_PROMO']);
        $price = intval($root['VLVENDA3']);
        if ($percent != null) {
            $new_sale_price = $price - (($percent/100) * $price);
            $data['sale_price'] = ''.$new_sale_price;
        }
    }

    // setta status
    switch ($root['STATUS']) {
        case 'PRODUC':
            $data['manage_stock'] = false;
            $data['stock_quantity'] = null;
            $data['stock_status'] = 'instock';
            break;
        case 'ESTOQU':
            $data['manage_stock'] = true;
            $data['stock_quantity'] = Acessar_quantidade_do_produto($var_row);
            $data['stock_status'] = 'instock';
            break;
        case 'INATIV':
            $data['manage_stock'] = false;
            $data['stock_quantity'] = null;
            $data['stock_status'] = 'outofstock';
            break;
    }

    // setta att tamanho
    if ($var_row['TAMANHO'] != null) {
        $data['attributes'][] = [
            'id' => $id_atributos[1]->id,
            "option" => $var_row['TAMANHO']
        ];
    } else {
        return;
    }
    // setta att cor
    if ($var_row['NOME'] != null) {
        $data['attributes'][] = [
            'id' => $id_atributos[0]->id,
            "option" => $var_row['NOME']
        ];
    } else {
        return;
    }

    return $data;
}

// DELETE varicao com base no sku
function Deletar_varicao_wooAPI($sku_produto) { 
    $prod_id = Buscar_produto_sku_wooAPI($sku_produto);
    if ($prod_id != null) {
        $result = Deletar_wooAPI('products/'.$prod_id[0]->parent_id.'/variations/'.$prod_id[0]->id);
        return $result;
    } else {
        Logging($funcao='Deletar_varicao_wooAPI, Sku: '.$sku_produto, $status='erro: variacao ('.$sku_produto.') nao estava inserida no site');
        return 3;
    }
}

// Altera o preco de um produto.
function Atualizar_precos_status_de_variacoes_wooAPI($id_root, $price, $sale_price, $status) {
    $variacoes = Buscar_wooAPI('products/'.$id_root->id.'/variations');
    $data_end = ['update' => []];
    $index = 0;
    foreach($variacoes as $row) {

        $data_end['update'][$index]['id'] = $row->id;
        // new price
        if ($price != null && $price != $row->regular_price) {
            $data_end['update'][$index]['regular_price'] = $price;
        }
        // new sale price
        if ($sale_price == -1) {
            $data_end['update'][$index]['sale_price'] = '';
        } else {
            $data_end['update'][$index]['sale_price'] = $sale_price;
        }
        
        // Caso seja necessario atualizar status, ele o faz
        if (($id_root->stock_status != 'instock' || $id_root->manage_stock != false) 
            || ($id_root->stock_status != 'outofstock' || $id_root->status != 'draft')
            || ($id_root->manage_stock != true) ) {
 
            $sku_splited = explode("-", $row->sku);
            $item_sku = $sku_splited[0];
            $numero_sku = $sku_splited[1];

            $sql = "SELECT * FROM item_lis 
                WHERE ITEM = ".$item_sku." and NUMERO = ".$numero_sku.";";
            $varQnt = Enviar_sql_bd($sql)[0];

            switch ($status) {
                case 'PRODUC':
                    $data_end['update'][$index]['manage_stock'] = false;
                    $data_end['update'][$index]['stock_quantity'] = null;
                    $data_end['update'][$index]['stock_status'] = 'instock';
                    break;
                case 'ESTOQU':
                    $data_end['update'][$index]['manage_stock'] = true;
                    $data_end['update'][$index]['stock_quantity'] = Acessar_quantidade_do_produto($varQnt);
                    $data_end['update'][$index]['stock_status'] = 'instock';
                    break;
                case 'INATIV':
                    $data_end['update'][$index]['manage_stock'] = false;
                    $data_end['update'][$index]['stock_quantity'] = null;
                    $data_end['update'][$index]['stock_status'] = 'outofstock';
                    break;
            }
        }

        if($index == 99) {
            Batch_wooAPI('products/'.$id_root->id.'/variations', $data_end);
            $data_end = ['update' => []];
            $index = 0;
        } else if (count($data_end['update'][$index]) > 0) {
            $index++;
        }
    }
    return Batch_wooAPI('products/'.$id_root->id.'/variations', $data_end);
}

// Calcula estoque
function Acessar_quantidade_do_produto($variation) {
    $sql = "SELECT (nr1.total - nr2.total) total 
        FROM (
            SELECT SUM(QTD_FINAL) total FROM ordem_lis 
            WHERE ITEM=".$variation['ITEM']." AND COR=".$variation['COR']." AND TAMANHO='".$variation['TAMANHO']."'
        ) nr1
        CROSS JOIN (
            SELECT SUM(QTD_INICIAL) total FROM pedido_lis
            WHERE ITEM=".$variation['ITEM']." AND COR=".$variation['COR']." AND TAMANHO='".$variation['TAMANHO']."'
        ) nr2";
    $qntFinal = Enviar_sql_bd($sql)[0]['total'];

    return intval($qntFinal);
}

/**
 * CRUD: Taxonomias de um produto
 * 
 * @func1: Inserir_taxonomia_wpAPI
 * @func2: Atualizar_taxonomia_wpAPI
 * @func3: Buscar_taxonomia_wpAPI
 * @func4: Buscar_taxonomia_pelo_idcaravell_wpAPI
 * @func5: Delete_taxonomia_wpAPI
 * @func6: Inserir_todas_toxonomias_disponiveis
 */
function Inserir_taxonomia_wpAPI($taxo_name, $id_caravell, $taxo_row) {
    try {
        include 'conex_wpapi.php';

        $params = [
            'name' => $taxo_row['NOME']
        ];
        $response = json_decode($wordpress->post('wp-json/wp/v2/'.$taxo_name, ['query' => $params])->getBody());
        $result = $wordpress->put('/wp-json/wp/v2/'.$taxo_name.'/id-caravell/put/'.$response->id.'/'.$id_caravell);
        return json_decode($result->getBody());
    } catch (Exception $e) {
        Logging($funcao='Inserir_taxonomia_wpAPI ==> @taxo: '.$taxo_name, $status=$e->getMessage());
    }
}

function Atualizar_taxonomia_wpAPI($taxo_name, $target, $taxo_row) {
    try {
        include 'conex_wpapi.php';

        $params = [
            'name' => $taxo_row['NOME']
        ];
        $response = $wordpress->put('wp-json/wp/v2/'.$taxo_name.'/'.$target, ['query' => $params]);
        return json_decode($response->getBody());
    } catch (Exception $e) {
        Logging($funcao='Atualizar_taxonomia_wpAPI ==> @taxo: '.$taxo_name, $status=$e->getMessage());
    }
}

function Buscar_taxonomia_wpAPI($taxo_name) {
    try {
        include 'conex_wpapi.php';

        $params = ['per_page' => 99];
        $response = $wordpress->get('wp-json/wp/v2/'.$taxo_name, ['query' => $params]);
        return json_decode($response->getBody());
    } catch (Exception $e) {
        Logging($funcao='Buscar_taxonomia_wpAPI - '.$taxo_name, $status=$e->getMessage());
    }
}

function Buscar_taxonomia_pelo_idcaravell_wpAPI($table_name, $target) {
    try {
        $linhas = Buscar_taxonomia_wpAPI($table_name);
        foreach ($linhas as $row) {
            if($row->id_caravell == $target) {
                return $row;
            }
        }
    } catch (Exception $e) {
        Logging($funcao='Buscar_taxonomia_pelo_idcaravell_wpAPI - '.$table_name, $status='erro');
    }
}

function Delete_taxonomia_wpAPI($taxo_name, $target) {
    try {
        include 'conex_wpapi.php';

        $params = ['force' => true];
        $response = $wordpress->delete('wp-json/wp/v2/'.$taxo_name.'/'.$target, ['query' => $params]);
        return json_decode($response->getBody());
    } catch (Exception $e) {
        Logging($funcao='Delete_taxonomia_wpAPI ==> @taxo: '.$taxo_name.' ==> @nome: '.$nome, $status=$e->getMessage());
    }
}

// @taxo_name: item_colecao / item_linha (tipos_de_roupas)
function Inserir_todas_toxonomias_disponiveis($table_caravell) {
    $result = [];
    if ($table_caravell == 'item_linha') {
        $taxo_name = 'tipos_de_roupas';
    } else if ($table_caravell == 'item_colecao') {
        $taxo_name = 'colecao';
    }
    $sql = "SELECT * FROM $table_caravell;";
    $itens = Enviar_sql_bd($sql);
    foreach ($itens as $row) {
        $result[] = Inserir_taxonomia_wpAPI($taxo_name, $row['NUMERO'], $row);
    }
    return $result;
}

/**
 * CRUD: Clientes
 * 
 * @func1: Inserir_atualizar_cliente_wooAPI
 * @func2: Verificar_usernames
 * @func3: Buscar_cliente_meta_wooAPI
 * @func4: Deletar_cliente_wooAPI
 * @func5: Deletar_todos_clientes_wooAPI
 */
// INSERT todos os cliente
// INSERT/ATUALIZAR cliente unico
function Inserir_atualizar_cliente_wooAPI($id_cliente=null, $is_update=null) {
    $sql = "SELECT c.NUMERO
            , c.NOME
            , c.TIPO
            , e.EMAIL
            , e.FONE1
            , e.FONE2
        FROM clientes c
        INNER JOIN enderecos e
            ON c.NUMERO = e.CLIENTE";
    if ($id_cliente == null) {
        $sql .= " WHERE c.TIPO = 7;";
    } else {
        $sql .= " WHERE c.NUMERO = ".$id_cliente.";";
    }
    $cliente = Enviar_sql_bd($sql);
    
    // Veririfica se o cliente alvo eh ou nao revendedor
    if ($cliente == null || count($cliente) <= 0) {
        if ($id_cliente != null) {
            Logging($funcao='Inserir_atualizar_cliente_wooAPI. Sku: '.$id_cliente, $status='Erro: cliente nao encontrado no banco.');
            return 2;
        } else {
            Logging($funcao='Inserir_atualizar_cliente_wooAPI', $status='Erro: tipo dos clientes difere de 7.');
            return;
        }
    }

    foreach ($cliente as $row) {
        // Cria todos os dados no formato wooapi
        $full_name = explode(' ', $row['NOME']);
        $email = $row['EMAIL'];
        $first_name = $last_name = $username = '';
        $ind = 0;
        foreach ($full_name as $row_name) {
            if ($ind == 0) {
                $first_name = $row_name;
                $username = $row_name;
                $ind++;
            } else {
                if ($ind == (count($full_name)-1)) {
                    $last_name .= $row_name;
                    $username .= '.'.$row_name;
                } else {
                    $last_name .= $row_name.' ';
                }
                $ind++;
            }
        }
        $username = strtolower($username);
        $username = Verificar_usernames($username);
        $password = Gerar_senha(14);

        if ($is_update != null) {
            $client_wooAPi = Buscar_cliente_meta_wooAPI($id_cliente);
            if ($client_wooAPi != null) {
                $data = [
                    'email'         => $email
                    , 'first_name'  => $first_name
                    , 'last_name'   => $last_name
                ];
                return Atualizar_wooAPI('customers/'.$client_wooAPi->id, $data);
            }
        } 
        
        $data = [
            'email' => $email
            , 'first_name'  => $first_name
            , 'last_name'   => $last_name
            , 'username'    => $username
            , 'password'    => $password
            , 'meta_data'   => [ 
                [
                    'key'       => 'id_caravell'
                    , 'value'   => $row['NUMERO']
                ]
            ]
        ];
        require_once 'templates/sign_up_confirmation.php';
        $result = Inserir_wooAPI('customers', $data);
        if ($result != null) {
            try {
                $msghtml = Render_template_sign_up_confirmation($first_name, $username, $email, $password);                
                Enviar_email($to=$email, $msghtml=$msghtml, $msgtxt='', $subject='Dados para acesso a Policorp');
                // $sbj='Dados para acesso em Policorp. Cliente:'.$row['NOME'].' (id: '.$row['NUMERO'].')';
                // Enviar_email($to='contato@seox.com.br', $msghtml=$msghtml, $msgtxt='', $subject=$sbj);
                
            } catch (Exception $e) {
                Logging($funcao='Inserir_atualizar_cliente_wooAPI', $status='erro: Nao foi possivel enviar email de confimacao.');
                Deletar_wooAPI('customers/'.$result->id);
                $result = 3;
            }
        } else {
            $msghtml = Render_template_sign_up_failed($first_name, $last_name, $email, $row['NUMERO']);
            $sbj='ERRO ao cadastrar revendedor: '.$row['NOME'].' (id: '.$row['NUMERO'].')';
            /**
             TROCAR hercilio's EMAIL por RICARDOR'S EMAIL
             */
            Enviar_email($to='hercilio@seox.com.br', $msghtml=$msghtml, $msgtxt='', $subject=$sbj);                    
        }
        if ($id_cliente != null) {
            return $result;
        }
    }
}

function Verificar_usernames($username_required) {
    include 'conex_wooapi.php';
    $clientes = $woocommerce->get('customers', array('search' => $username_required));
    $index = 1;
    $new_name = $username_required;
    while ($clientes != null and count($clientes) > 0) {
        $new_name = $username_required.'.'.$index;
        $clientes = $woocommerce->get('customers', array('search' => $new_name));
        $index++;
    }
    return $new_name;
}

// GET cliente com base no meta data 
function Buscar_cliente_meta_wooAPI($id_caravell) {    
    $index = 1;
    while (true) {
        $clientes = Buscar_wooAPI('customers', $index);
        if(count($clientes) == 0) {
            Logging($funcao='Buscar_cliente_meta_wooAPI. Id_caravell: '.$id_caravell, $status='erro: Nao foi possivel encontrar id_caravell');
            return;
        }
        foreach ($clientes as $row) {
            $meta = $row->meta_data;
            if (empty($meta)) {
                continue;
            }
            if ($meta[0]->value == $id_caravell) {
                $result = Buscar_id_wooAPI('customers', $row->id);
                return $result;
            }
        }
        $index++;
    }
}

// DELETE cliente com base no meta data
function Deletar_cliente_wooAPI($id_caravell) { 
    $client = Buscar_cliente_meta_wooAPI($id_caravell);
    if ($client != null) {
        $result = Deletar_wooAPI('customers/'.$client->id);
        return $result;
    } else {
        Logging($funcao='Deletar_cliente_wooAPI', $status='erro: tipo do cliente difere de revendedora');
    }
}

function Deletar_todos_clientes_wooAPI() {
    while (true) {
        $data = ['delete' => []];
        $cli = Buscar_wooAPI('customers');
        if(count($cli) == 0) {
            return true;
        }
        foreach ($cli as $key => $row) {
            $data['delete'][$key] = $row->id;
        }
        Batch_wooAPI('customers', $data);
    }
}

/**
 * CRUD PEDIDOS
 * 
 * @func1: Inserir_atualizar_pedido_do_caravell_para_site_wooAPI
 * @func2: Inserir_todos_pedidos_disponiveis
 * @func3: Buscar_pedido_pelo_id_do_cliente_wooAPI
 * @func4: Deletar_itens_em_um_pedido
 * @func5: Deletar_todos_pedidos_de_um_cliente
 * @func6: Deletar_todos_pedidos_wooAPI
 */
// Usando para inserir ou atualizar um produto criado no banco caravell (produtos dividos)
function Inserir_atualizar_pedido_do_caravell_para_site_wooAPI($ped_caravell, $update=false, $client_id=null) {

    if ($client_id == null) {
        $client_id = Buscar_cliente_meta_wooAPI($ped_caravell['CLIENTE']);
        if ($client_id != null) {
            $client_id = $client_id->id;
        } else {
            Logging($funcao='Inserir_atualizar_pedido_do_caravell_para_site_wooAPI. Sku: '.$ped_caravell['NUMERO']
                    , $status='erro: cliente que realizou pedido nao esta inserido no Site');
            return 3;
        }
    }
    // Cata a lista de variacoes requeridas
    $sql = "SELECT * FROM pedido_lis WHERE PEDIDO = ".$ped_caravell['NUMERO'].";";
    $pedido_lis = Enviar_sql_bd($sql);
    // Cria a data de variacoes requeridas pelo pedido
    $data_itens = [];
    foreach($pedido_lis as $row) {
        $sql = "SELECT * FROM item_lis WHERE ITEM = ".$row['ITEM']." AND COR = ".$row['COR']." AND TAMANHO = \"".$row['TAMANHO']."\";";
        $item_lis = Enviar_sql_bd($sql);

        if ($item_lis == null || count($item_lis) <= 0) {
            Logging($funcao='Inserir_atualizar_pedido_do_caravell_para_site_wooAPI. Sku: '.$ped_caravell['NUMERO']
                , $status='erro: variacao do pedido requisitada nao existe no banco. Impossivel registrar pedido no site');
            return 2;
        } 
        // somente utiliza client_id o inserir todos disponiveis, e como a pesquisa sql verifica se a variacao esta inserida, nao eh necessario essa verificacao
        if ($client_id == null) {
            $id_var_wooAPI = Buscar_produto_sku_wooAPI($item_lis[0]['ITEM'].'-'.$item_lis[0]['NUMERO']);
            if ($id_var_wooAPI == null || !Verificar_wooAPI('products/'.$id_var_wooAPI[0]->parent_id.'/variations', $id_var_wooAPI[0]->id)) {
                Logging($funcao='Inserir_atualizar_pedido_do_caravell_para_site_wooAPI. Sku: '.$ped_caravell['NUMERO']
                    , $status='erro: variacao nao esta inserida no site.');
                return 3;
            }
        }

        if ($ped_caravell['ATENDIDO'] == 1) {
            $data_itens[] = [
                'quantity'  => $row['QTD_FINAL']
                , 'sku'     => $item_lis[0]['ITEM'].'-'.$item_lis[0]['NUMERO']
            ];
        } else {
            $data_itens[] = [
                'quantity'  => $row['QTD_INICIAL']
                , 'sku'     => $item_lis[0]['ITEM'].'-'.$item_lis[0]['NUMERO']
            ];
        }
    }
    // Setta staus com base no bd caravell
    if ($ped_caravell['ATENDIDO'] == 0) {
        $status = 'processing';
    } else {
        $status = 'completed';   
    }
    // Cria estrutura json 
    $data = [
        'status'            => $status
        , 'total'           => $ped_caravell['VLTOTAL']
        , 'customer_note'   => $ped_caravell['OBS']
        , 'customer_id'     => $client_id
        , 'line_items'      => $data_itens
        , 'meta_data'   => [ 
            [
                'key'       => 'id_caravell'
                , 'value'   => $ped_caravell['NUMERO']
            ]
        ]
    ];

    // Verifica se é um insercao ou uma atualizacao
    if ($update) {
        if (Verificar_wooAPI('orders', $ped_caravell['id_woocommerce'])) {
            Deletar_itens_em_um_pedido($ped_caravell['id_woocommerce']);
            $woo_response = Atualizar_wooAPI('orders/'.$ped_caravell['id_woocommerce'], $data);
        } else {
            Logging($funcao='Inserir_atualizar_pedido_do_caravell_para_site_wooAPI', $status='erro: id_woocommerce nao corresponde a um pedido no site.');
            return 3;
        }        
    } else {
        $woo_response = Inserir_wooAPI('orders', $data);
        Atualizar_bd($tabela='pedido', $colum_id='NUMERO', $id=$ped_caravell['NUMERO'], $coluna ='id_woocommerce', $dado=$woo_response->id);
    }
    return $woo_response;
}

function Inserir_todos_pedidos_disponiveis() {
    $clientes = Buscar_wooAPI('customers');
    if ($clientes == null || count($clientes) <= 0) {
        Logging($funcao='Inserir_todos_pedidos_disponiveis', $status='Erro: nao ha clientes inseridos no site.');
        return;
    }

    foreach ($clientes as $cli_row) {
        $sql = "SELECT * FROM pedido WHERE CLIENTE = ".$cli_row->meta_data[0]->value.";";
        $sql = "SELECT p.NUMERO , p.ATENDIDO , p.CLIENTE , p.VLTOTAL , p.OBS , p.id_woocommerce
            FROM pedido p 
            INNER JOIN pedido_lis pl 
                ON p.NUMERO = pl.PEDIDO 
            WHERE p.CLIENTE = ".$cli_row->meta_data[0]->value." AND p.NUMERO NOT IN (
                SELECT pl.PEDIDO
                FROM pedido_lis pl
                INNER JOIN itens i
                    ON pl.ITEM = i.NUMERO
                WHERE i.STATUS IS NULL
            )
            GROUP BY p.NUMERO";
        $pedidos = Enviar_sql_bd($sql);

        foreach ($pedidos as $ped_row) {
            Inserir_atualizar_pedido_do_caravell_para_site_wooAPI($ped_caravell=$ped_row, $update=false, $client_id=$cli_row->id);
        }
    }
}

function Buscar_pedido_pelo_id_do_cliente_wooAPI($id_cliente) {
    include 'conex_wooapi.php';
    
    try {
        $result = $woocommerce->get('orders', array('customer' => $id_cliente));
        return $result;
    } catch (Exception $e) {
        Logging($funcao='Buscar_pedido_pelo_id_do_cliente_wooAPI. Id_cliente: '.$id_cliente, $status=$e->getMessage());
    } 
}

// Utilizado para deletar todos os itens de um pedido (utiliza o plugin seox-deleting-itens-in-order)
function Deletar_itens_em_um_pedido($id_wooapi_pedido) {
    include 'conex_wpapi.php';

    try {
        $response = $wordpress->delete('wp-json/wp/v2/orders/itens/del/'.$id_wooapi_pedido);
        return $response;
    } catch (Exception $e) {
        Logging($funcao='Deletar_itens_em_um_pedido', $status=$e->getMessage());
    }  
}

function Deletar_todos_pedidos_de_um_cliente($id_cliente) {
    while (true) {
        $data = ['delete' => []];
        $ped = Buscar_pedido_pelo_id_do_cliente_wooAPI($id_cliente);
        if(count($ped) == 0) {
            return true;
        }
        foreach ($ped as $key => $row) {
            $data['delete'][$key] = $row->id;
        }
        Batch_wooAPI('orders', $data);
    }
}

function Deletar_todos_pedidos_wooAPI() {
    while (true) {
        $data = ['delete' => []];
        $ped = Buscar_wooAPI('orders');
        if(count($ped) == 0) {
            return true;
        }
        foreach ($ped as $key => $row) {
            $data['delete'][$key] = $row->id;
        }
        Batch_wooAPI('orders', $data);
    }
}


/*****************************
 * FUNCOES DO BANCO DE DADOS *
 *****************************/
/**
 * CRUDO BASIC MYSQL DATABASE
 * 
 * @func1: Enviar_sql_bd
 * @func2: Selecionar_itens_not_null_bd
 * @func3: Selecionar_id_bd
 * @func4: Atualizar_bd
 * @func5: Atualizar_grupo_bd
 * @func6: Deletar_sql_bd
 * @func7: Testar_conexao_bd
 */
// Envia todas as requisicoes de sql para servidor
function Enviar_sql_bd($sql) {
    include 'conex_bd.php';
    
    try {
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    } catch (Exception $e) {
        Logging($funcao='Enviar_sql_bd, sql: '.$sql, $status=$e->getMessage());
    }  
}

function Selecionar_itens_not_null_bd() {
    $sql = "SELECT * FROM itens WHERE STATUS IS NOT NULL ORDER BY NUMERO ASC;";
    return Enviar_sql_bd($sql);
}

function Selecionar_id_bd($tabela, $id) {
    $sql = "SELECT * FROM $tabela WHERE NUMERO='$id'";
    return Enviar_sql_bd($sql);     
}

function Atualizar_bd($tabela, $colum_id, $id, $coluna, $dado) {
    include 'conex_bd.php';
    
    try {
        $sql = "UPDATE $tabela SET $coluna=$dado WHERE $colum_id = $id";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
    } catch (Exception $e) {
        Logging($funcao='Enviar_sql_bd, sql: '.$sql, $status=$e->getMessage());
    } 
}

function Atualizar_grupo_bd($sql) {
    include 'conex_bd.php';
    
    try {
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
    } catch (Exception $e) {
        Logging($funcao='Atualizar_grupo_bd, sql: '.$sql, $status=$e->getMessage());
    } 
}

function Deletar_sql_bd($sql) {
    include 'conex_bd.php';
    
    try {
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
    } catch (Exception $e) {
        Logging($funcao='Deletar_sql_bd, sql: '.$sql, $status=$e->getMessage());
    } 
}

function Testar_conexao_bd() {
	try {
		include 'conex_bd.php';
	} catch (Exception $e) {
        Logging($funcao='Testar_conexao_bd - OFF', $status=$e->getMessage());
        return false;
    } 
    return true;
}


/*****************************
 *          SCRIPTS          *
 *****************************/
/**
 * POPULATE SITE
 * 
 * @func1: Deletar_todos_dados_site
 * @func2: Inserir_taxos_init
 * @func3: Inserir_produtos_init
 * @func4: Inserir_atributos_init
 * @func5: Inserir_variacoes_init
 * @func6: Inserir_clientes_init
 * @func7: Inserir_pedidos_init
 * @func8: Script_inicial
 */
function Deletar_todos_dados_site() {
    // 0 - Delecao de todos os produtos da loja
    try {
        // Del produtos/variacoes
        Deletar_todos_produtos_wooAPI();
        // Del taxonomias
        while (true) {
            $tip_roupas = Buscar_taxonomia_wpAPI('tipos_de_roupas');
            if(count($tip_roupas) == 0) {
                break;
            }
            foreach ($tip_roupas as $row) {
                Delete_taxonomia_wpAPI('tipos_de_roupas', $row->id);
            }
        }
        while (true) {
            $colecao = Buscar_taxonomia_wpAPI('colecao');
            if(count($colecao) == 0) {
                break;
            }
            foreach ($colecao as $row) {
                Delete_taxonomia_wpAPI('colecao', $row->id);
            }
        }
        // Del atributos
        while (true) {
            $att = Buscar_wooAPI('products/attributes');
            if(count($att) == 0) {
                break;
            }
            foreach ($att as $row) {
                Deletar_wooAPI('products/attributes/'.$row->id);
            }
        }
        // Del clientes
        Deletar_todos_clientes_wooAPI();
        // Del pedidos
        Deletar_todos_pedidos_wooAPI();
    } catch (Exception $e) {
        Logging($funcao='Deletar_todos_dados_site', $status='0 - Delecoes: ERRO '.$e->getMessage());
    } finally {
        Logging($funcao='Deletar_todos_dados_site', $status='0 - Delecoes: realizado com sucesso!');
    }
}

// Insere todas as taxonomias (evita bug)
function Inserir_taxos_init() {
    try {
        Inserir_todas_toxonomias_disponiveis($taxo_name='item_colecao');
        Inserir_todas_toxonomias_disponiveis($taxo_name='item_linha');
    } catch (Exception $e) {
        Logging($funcao='Script_inicial', $status='1 - inserir taxonomias: ERRO '.$e->getMessage());
    } finally {
        Logging($funcao='Script_inicial', $status='1 - inserir taxonomias: realizado com sucesso!');
    }
}

// Insere todos os produtos (bug detectado aqui)
function Inserir_produtos_init() {
    $products = Buscar_wooAPI('products');
    if (!$products) {
        try {
            Inserir_todos_produtos_disponiveis();
        } catch (Exception $e) {
            Logging($funcao='Inserir_produtos_init', $status='2 - inserir produtos: ERRO '.$e->getMessage());
        } finally {
            Logging($funcao='Inserir_produtos_init', $status='2 - inserir produtos: realizado com sucesso!');
        }
    } 
}

// Insere todos os atributos (bug detectado aqui tambem)
function Inserir_atributos_init() {
    $att = Buscar_wooAPI('products/attributes');
    if (!$att) {
        // 3 - inserir atributos:
        try {
            Inserir_todos_atributos_de_produtos_disponiveis();
        } catch (Exception $e) {
            Logging($funcao='Script_inicial', $status='3 - inserir atributos: ERRO '.$e->getMessage());
        } finally {
            Logging($funcao='Script_inicial', $status='3 - inserir atributos: realizado com sucesso!');
        }
    }
}

// Insere todos os variacoes (bug detectado aqui tambem)
function Inserir_variacoes_init() {
    $page = 1;
    $okay = true;
    $woo_prods = Buscar_wooAPI('products', $page);
    while ($woo_prods) {
        foreach ($woo_prods as $key => $value) {
            if ($value->variations) {
                $okay = false;
            }
        }
        $page++;
        $woo_prods = Buscar_wooAPI('products', $page);
    }
    if ($okay) {
        // 4 - inserir variacoes:
        try {
            Inserir_todas_variacoes_de_produtos_disponiveis();
        } catch (Exception $e) {
            Logging($funcao='Inserir_variacoes_init', $status='4 - inserir variacoes: ERRO '.$e->getMessage());
        } finally {
            Logging($funcao='Inserir_variacoes_init', $status='4 - inserir variacoes: realizado com sucesso!');
        }
    }
}

// Insere todos os clientes (bug detectado aqui tambem)
function Inserir_clientes_init() {
    $clients = Buscar_wooAPI('customers');
    if (!$clients) {
        // 5 - inserir clientes:
        try {
            Inserir_atualizar_cliente_wooAPI();
        } catch (Exception $e) {
            Logging($funcao='Inserir_clientes_init', $status='5 - inserir clientes: ERRO '.$e->getMessage());
        } finally {
            Logging($funcao='Inserir_clientes_init', $status='5 - inserir clientes: realizado com sucesso!');
        }
    }
}

// Insere todos os pedidos (bug detectado aqui tambem)
function Inserir_pedidos_init() {
    $pedidos = Buscar_wooAPI('orders');
    if (!$pedidos) {
        // 6 - inserir pedidos:
        try {
            Inserir_todos_pedidos_disponiveis();
        } catch (Exception $e) {
            Logging($funcao='Inserir_pedidos_init', $status='6 - inserir pedidos: ERRO '.$e->getMessage());
        } finally {
            Logging($funcao='Inserir_pedidos_init', $status='6 - inserir pedidos: realizado com sucesso!');
        }
    }
}

// Isere todo o conteudo do site
function Script_init() {
    // 7 - truncando tabela modificacoes:
    try {
        $sql = "TRUNCATE TABLE modificacoes;";
        Enviar_sql_bd($sql);
    } catch (Exception $e) {
        Logging($funcao='Script_inicial', $status='7 - truncando tabela modificacoes: ERRO');
    } finally {
        Logging($funcao='Script_inicial', $status='7 - truncando tabela modificacoes: realizado com sucesso!');
    }
}

/**
 * UPDATE SITE
 * 
 * @func1: Update_site
 * 
 * @result = 2: algum erro no banco caravell impossibilitou a execucao
 * @result = 3: algum erro na api woocommerce impossibilitou a execucao 
 * @result = 4: tentativa de inserir algo que jah esta inserido
 */
function Update_site() {
	if (Testar_conexao_bd() == false) {
		return;
	}

    $sql = "SELECT * FROM cronjob_control;";
    $status = Enviar_sql_bd($sql);
    if ($status[0]['status'] == 1) {
        Logging($funcao='Update_site. Cron OFF', $status='Cron job jah esta trabalhando.');
        return;
    }

    $sql = "UPDATE cronjob_control SET status = 1;";
    Atualizar_grupo_bd($sql);

    $sql = "DELETE a FROM modificacoes AS a, modificacoes AS b WHERE a.id_tabela=b.id_tabela and a.tabela=b.tabela AND a.acao=b.acao AND a.status=0 AND a.id < b.id";
    Deletar_sql_bd($sql);

    $sql = "SELECT * FROM modificacoes WHERE status = 0;";
    $modificacoes = Enviar_sql_bd($sql);
    // Inicia a leitura da tabela modificacoes
    foreach ($modificacoes as $key => $mod_row) {
        $result = null;
        switch ($mod_row['tabela']) {

            // Caso seja uma modificacao em produtos
            case 'itens':
                if ($mod_row['acao'] == 'DELETE') {
                    $result = Deletar_produto_wooAPI($mod_row['id_tabela']);
                } else {
                    $sql = "SELECT * FROM itens WHERE NUMERO = ".$mod_row['id_tabela'].";";
                    $prod_root = Enviar_sql_bd($sql);
                    // Caso linha tenha sido deletada
                    if ($prod_root == null || count($prod_root) <= 0) {
                        Logging($funcao='Update_site. Itens - id_caravell: '.$mod_row['id_tabela'], $status='erro: produto nao esta registrado no banco.');
                        $result = 2;
                        break;
                    }

                    if ($mod_row['acao'] == 'INSERT') {
                        $result = Inserir_todos_produtos_disponiveis($mod_row['id_tabela']);
                    } else if ($mod_row['acao'] == 'UPDATE') {
                        $result = Inserir_todos_produtos_disponiveis($mod_row['id_tabela'], $is_update=true);
                    } 
                }
                if ($result == null) {
                    Logging($funcao='Update_site. Itens - id_caravell: '.$mod_row['id_tabela'], $status='erro: ao '.$mod_row['acao'].' produto');
                    $result = 3;
                }
                break;

            // Caso seja uma modificacao em variacoes
            case 'item_lis':
                $id_var = explode(".", $mod_row['id_tabela']);
                if ($mod_row['acao'] == 'DELETE') {
                    $result = Deletar_varicao_wooAPI($id_var[0].'-'.$id_var[1]);
                } else {
                    $sql = "SELECT * FROM item_lis WHERE ITEM = ".$id_var[0]." AND NUMERO = ".$id_var[1].";";
                    $var_root = Enviar_sql_bd($sql);
                    // Caso linha tenha sido deletada
                    if ($var_root == null || count($var_root) <= 0) {
                        Logging($funcao='Update_site. Item_lis - id_caravell: '.$mod_row['id_tabela'], $status='erro: variacoes nao estao registradas no banco.');
                        $result = 2;
                        break;
                    }

                    if ($mod_row['acao'] == 'INSERT') {
                        $result = Inserir_atualizar_variacao_registrada_wooAPI($id_var[0].'-'.$id_var[1]);
                    } else if ($mod_row['acao'] == 'UPDATE') {
                        $result = Inserir_atualizar_variacao_registrada_wooAPI($id_var[0].'-'.$id_var[1], $is_update=true);
                    }
                }
                if ($result == null) {
                    Logging($funcao='Update_site. Item_lis - id_caravell: '.$mod_row['id_tabela'], $status='erro: ao '.$mod_row['acao'].' variacoes');
                    $result = 3;
                }
                break;

                // Caso seja uma modificacao em variacoes
            case 'update_qnt':
                $id_var = explode(".", $mod_row['id_tabela']);
                $sql = "SELECT * FROM item_lis WHERE ITEM = ".$id_var[0]." AND NUMERO = ".$id_var[1].";";
                $var_root = Enviar_sql_bd($sql);
                // Caso linha tenha sido deletada
                if ($var_root == null || count($var_root) <= 0) {
                    Logging($funcao='Update_site. Update_qtd - id_caravell: '.$mod_row['id_tabela'], $status='erro: variacoes nao estao registradas no banco.');
                    $result = 2;
                    break;
                }

                $result = Inserir_atualizar_variacao_registrada_wooAPI($id_var[0].'-'.$id_var[1], $is_update=true);
                if ($result == null) {
                    Logging($funcao='Update_site. '.$mod_row['tabela'].' - id_caravell: '.$mod_row['id_tabela'], $status='erro: ao '.$mod_row['acao'].' variacoes');
                    $result = 3;
                }
                break;

             // Caso seja uma modificacao em cor / tecido
             case 'cores':
                $atribute_id = Buscar_wooAPI('products/attributes')[0]->id;
                $id_and_name = explode('.', $mod_row['id_tabela']);
                // Utilizado para acessar id da cor / tecido no site
                $target = Buscar_cor_tecido_id_pelo_nome_wooAPI($id_and_name[1], $atribute_id);
                if ($target == null) {
                    Logging($funcao='Update_site. Cores - id_caravell: '.$mod_row['id_tabela'], $status='erro: cor que deseja-se deletar nao esta sendo utilziada por nenhum item ativo do site.');
                    $result = 3;
                    break;
                }
                 
                $sql = "SELECT * FROM cores WHERE NUMERO = ".$id_and_name[0].";";
                $cor_root = Enviar_sql_bd($sql);
                // Caso linha tenha sido deletada
                if ($cor_root == null || count($cor_root) <= 0) {
                    Logging($funcao='Update_site. Cores - id_caravell: '.$mod_row['id_tabela'], $status='erro: cor desejada nao esta registrada no banco.');
                    $result = 2;
                    break;
                }

                if ($mod_row['acao'] == 'UPDATE') {
                    $result = Atualizar_wooAPI('products/attributes/'.$atribute_id.'/terms/'.$target->id, ['name' => $cor_root[0]['NOME']]); 
                }
                if ($result == null) {
                    Logging($funcao='Update_site. Cores - id_caravell: '.$mod_row['id_tabela'], $status='erro: ao '.$mod_row['acao'].' cor.');
                    $result = 3;
                }
                break;
            
            // Caso seja uma modificacao em tipos de roupas
            case 'item_linha':
                if ($mod_row['acao'] == 'DELETE') {
                    $target = Buscar_taxonomia_pelo_idcaravell_wpAPI('tipos_de_roupas', $mod_row['id_tabela']);
                    if ($target == null) {
                        Logging($funcao='Update_site. Item_linha - id_caravell: '.$mod_row['id_tabela'], $status='erro: deletar taxonomia ja nao registrada no site.');
                        $result = 3;
                    } else {
                        $result = Delete_taxonomia_wpAPI('tipos_de_roupas', $target->id);
                    }
                } else {
                    $sql = "SELECT * FROM item_linha WHERE NUMERO = ".$mod_row['id_tabela'].";";
                    $tip_root = Enviar_sql_bd($sql);
                    // Caso linha tenha sido deletada
                    if ($tip_root == null || count($tip_root) <= 0) {
                        Logging($funcao='Update_site. Item_linha - id_caravell: '.$mod_row['id_tabela'], $status='erro: item_linha desejado nao esta registrada no banco.');
                        $result = 2;
                        break;
                    }

                    if ($mod_row['acao'] == 'INSERT') {
                        $result = Inserir_taxonomia_wpAPI('tipos_de_roupas', $mod_row['id_tabela'], $tip_root[0]);
                    } else if ($mod_row['acao'] == 'UPDATE') {
                        $target = Buscar_taxonomia_pelo_idcaravell_wpAPI('tipos_de_roupas', $mod_row['id_tabela']);
                        if ($target == null) {
                            Logging($funcao='Update_site. Item_linha - id_caravell: '.$mod_row['id_tabela'], $status='erro: atualizar taxonomia nao registrada no site.');
                            $result = 3;
                        } else {
                            $result = Atualizar_taxonomia_wpAPI('tipos_de_roupas', $target->id, $tip_root[0]);  
                        }      
                    }  
                }
                if ($result == null) {
                    Logging($funcao='Update_site. Item_linha - id_caravell: '.$mod_row['id_tabela'], $status='erro: ao '.$mod_row['acao'].' item_linha.');
                    $result = 3;
                }
                break;    

            // Caso seja uma modificacao em colecao
            case 'item_colecao':
                if ($mod_row['acao'] == 'DELETE') {
                    $target = Buscar_taxonomia_pelo_idcaravell_wpAPI('colecao', $mod_row['id_tabela']);
                    if ($target == null) {
                        Logging($funcao='Update_site. Item_colecao - id_caravell: '.$mod_row['id_tabela'], $status='erro: deletar taxonomia ja nao registrada no site.');
                        $result = 3;
                    } else {
                        $result = Delete_taxonomia_wpAPI('colecao', $target->id);
                    }
                } else {
                    $sql = "SELECT * FROM item_colecao WHERE NUMERO = ".$mod_row['id_tabela'].";";
                    $col_root = Enviar_sql_bd($sql);
                    // Caso linha tenha sido deletada
                    if ($col_root == null || count($col_root) <= 0) {
                        Logging($funcao='Update_site. Item_colecao - id_caravell: '.$mod_row['id_tabela'], $status='erro: item_colecao desejado nao esta registrada no banco.');
                        $result = 2;
                        break;
                    }

                    if ($mod_row['acao'] == 'INSERT') {
                        $result = Inserir_taxonomia_wpAPI('colecao', $mod_row['id_tabela'], $col_root[0]);
                    } else if ($mod_row['acao'] == 'UPDATE') {
                        $target = Buscar_taxonomia_pelo_idcaravell_wpAPI('colecao', $mod_row['id_tabela']);
                        if ($target == null) {
                            Logging($funcao='Update_site. Item_colecao - id_caravell: '.$mod_row['id_tabela'], $status='erro: atualizar taxonomia nao registrada no site.');
                            $result = 3;
                        } else {
                            $result = Atualizar_taxonomia_wpAPI('colecao', $target->id, $col_root[0]);  
                        }      
                    }  
                }
                if ($result == null) {
                    Logging($funcao='Update_site. Item_colecao - id_caravell: '.$mod_row['id_tabela'], $status='erro: ao '.$mod_row['acao'].' item_colecao.');
                    $result = 3;
                }
                break;
                
            // Caso seja uma modificacao em clientes
            case 'clientes':
                if ($mod_row['acao'] == 'DELETE') {
                    $client = Buscar_cliente_meta_wooAPI($mod_row['id_tabela']);
                    Deletar_todos_pedidos_de_um_cliente($client->id);
                    $result = Deletar_wooAPI('customers/'.$client->id);
                } else {
                    $sql = "SELECT * FROM clientes WHERE NUMERO = ".$mod_row['id_tabela'].";";
                    $cli_root = Enviar_sql_bd($sql);
                    // Caso linha tenha sido deletada
                    if ($cli_root == null || count($cli_root) <= 0) {
                        Logging($funcao='Update_site. Clientes - id_caravell: '.$mod_row['id_tabela'], $status='erro: cliente desejado nao esta registrada no banco.');
                        $result = 2;
                        break;
                    }

                    if ($mod_row['acao'] == 'INSERT') {
                        $result = Inserir_atualizar_cliente_wooAPI($id_cliente=$mod_row['id_tabela']);
                    } else if ($mod_row['acao'] == 'UPDATE') {
                        $result = Inserir_atualizar_cliente_wooAPI($id_cliente=$mod_row['id_tabela'], $is_update=true);
                    }
                }
                if ($result == null) {
                    Logging($funcao='Update_site. Clientes - id_caravell: '.$mod_row['id_tabela'], $status='erro: ao '.$mod_row['acao'].' clientes.');
                    $result = 3;
                }
                break;
            
            // Caso seja uma modificacao em pedido
            case 'pedido':
                if ($mod_row['acao'] == 'DELETE') {
                    $id_woo = explode(".", $mod_row['id_tabela'])[1];
                    if (!($id_woo == '' || $id_woo == null) && Verificar_wooAPI('orders', $id_woo)) {
                        $result = Deletar_wooAPI('orders/'.$id_woo);
                    }
                } else {
                    $sql = "SELECT * FROM pedido WHERE NUMERO = ".$mod_row['id_tabela'].";";
                    $ped_root = Enviar_sql_bd($sql);
                    // Caso pedido tenha sido deletado
                    if ($ped_root == null || count($ped_root) <= 0) {
                        Logging($funcao='Update_site. Pedido - id_caravell: '.$mod_row['id_tabela'], $status='erro: pedido nao esta no banco caravell.');
                        $result = 2;
                        break;
                    }
                    
                    if ($mod_row['acao'] == 'INSERT') {
                        if ($ped_root[0]['id_woocommerce'] == null) {
                            $result = Inserir_atualizar_pedido_do_caravell_para_site_wooAPI($ped_caravell=$ped_root[0]); 
                        } else {
                            $result = 4;
                        }
                    } else if ($mod_row['acao'] == 'UPDATE') {
                        if ($ped_root[0]['id_woocommerce'] != null) {
                            $result = Inserir_atualizar_pedido_do_caravell_para_site_wooAPI($ped_caravell=$ped_root[0], $update=true);
                        }
                        // Caso nao tenha id_woocommerce, isto quer dizer que e eh uma divisao de pedido
                        else {
                            $result = Inserir_atualizar_pedido_do_caravell_para_site_wooAPI($ped_caravell=$ped_root[0]);
                        }     
                    } 
                }
                if ($result == null) {
                    Logging($funcao='Update_site. Pedido - id_caravell: '.$mod_row['id_tabela'], $status='erro: ao '.$mod_row['acao'].' pedido/pedido_lis.');
                    $result = 3;
                }
                break;

            case 'pedido_lis':
                $sql = "SELECT * FROM pedido WHERE NUMERO = ".$mod_row['id_tabela'].";";
                $ped_root = Enviar_sql_bd($sql);

                // Caso pedido tenha sido deletado
                if ($ped_root == null || count($ped_root) <= 0) {
                    // pedido_lis cadastra-se sempre como update
                    // Se o pedido root nao existe mais na tabela, isto quer dizer q todos os pedido_lis foram deletados
                    if ($mod_row['tabela'] == 'pedido_lis') {
                        $result = 1;
                    } else {
                        Logging($funcao='Update_site. Pedido_lis - id_caravell: '.$mod_row['id_tabela'], $status='erro: pedido nao esta no banco caravell.');
                        $result = 2;
                    }
                    break;
                }
                
                if ($ped_root[0]['id_woocommerce'] != null) {
                    $result = Inserir_atualizar_pedido_do_caravell_para_site_wooAPI($ped_caravell=$ped_root[0], $update=true);
                }
                // Caso nao tenha id_woocommerce, isto quer dizer que e eh uma divisao de pedido
                else {
                    $result = Inserir_atualizar_pedido_do_caravell_para_site_wooAPI($ped_caravell=$ped_root[0]);
                }     
                 
                if ($result == null) {
                    Logging($funcao='Update_site. Pedido_lis - id_caravell: '.$mod_row['id_tabela'], $status='erro: ao '.$mod_row['acao'].' pedido/pedido_lis.');
                    $result = 3;
                }
                break;
        }

        if ($result != null) {
            if (is_int($result) && $result > 1 && $result < 4) {
                Atualizar_bd($tabela='modificacoes', $colum_id='id', $id=$mod_row['id'], $coluna='status', $dado=$result);
            } else {
                Atualizar_bd($tabela='modificacoes', $colum_id='id', $id=$mod_row['id'], $coluna='status', $dado=1);
            }
        }
    }
    $sql = "UPDATE cronjob_control SET status = 0;";
    Atualizar_grupo_bd($sql);
}


/*****************************
 *   FUNCOES DE ASSITENCIA   *
 *****************************/
/**
 * CREATE LOG MESSAGE
 * 
 * @fun1: Logging
 */
function Logging($funcao, $status, $obs=null) {
    header('Content-Type: text/html; charset=utf-8');
    $data_log = date("d_m_Y"); 
    $arquivo = "functions/logs/Log_$data_log.txt";
    $texto = "Funcao= ".$funcao." , Status= ".$status." , Obs= ".$obs.";\r\n"; 
    $manipular = fopen($arquivo, "a+"); 
    fwrite($manipular, $texto); 
    fclose($manipular); 
}

/**
 * GENERATE RANDOM PASSWORD
 * 
 * @fun1: Gerar_senha
 */
function Gerar_senha($qtd_caracter) {
	$aleatorio = rand($qtd_caracter, $qtd_caracter); // 12 a 12 CARACTERES
	$valor = substr(str_shuffle("ABCDEFGHIJKLMNPQRSTUVYXWZabcdefghijklmnopqrstuvyxwz0123456789"), 0, $aleatorio);
	return $valor;	
}

/**
 * SEND ELASTIC MAIL
 * 
 * @fun1: Enviar_email
 */
//campos obrigatorios = $from,$to,$key,$msghtml,$msgtxt,$isTransactional,$sender,$subject
function Enviar_email($to, $msghtml, $msgtxt, $subject){    
    $from = 'naoresponda@seox.com.br';
    $key = '823d6a87-e548-4d8a-9d6e-57d39be13fbe';
    $isTransactional = 'true';
    $sender = $from;
    $template = null;

	$result=array();
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://api.elasticemail.com/v2/email/send");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, true);

	$data = array(
	    'apikey' => $key,
	    'bodyText' => $msgtxt,
	    'bodyHtml' => $msghtml,
	    'from' => $from,
	    'isTransactional' => $isTransactional,
	    'sender' => $sender,
	    'to' => $to,
	    'template' => $template,
	    'subject' => $subject
	);

	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	$output = curl_exec($ch);
	$info = curl_getinfo($ch);
	curl_close($ch);
	
	if ($output) {
		$obj = json_decode($output);
		$result['status']=true;
		$result['transactionid']=$obj->data->transactionid;
		$result['messageid']=$obj->data->messageid;
		return $result;
	} else {
        Logging($funcao='Enviar_email', $status=curl_error($ch));
    }
    $result['status']=false;
    $result['transactionid']=null;
    $result['messageid']=null;
    return $result;
}

?>